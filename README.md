```text
 ████     ████ ██ ████     ██ ██   ██  ████     ██ 
░██░██   ██░██░██░██░██   ░██░██  ██  █░░░██   █░█ 
░██░░██ ██ ░██░██░██░░██  ░██░██ ██  ░█  █░█  █ ░█ 
░██ ░░███  ░██░██░██ ░░██ ░██░████   ░█ █ ░█ ██████
░██  ░░█   ░██░██░██  ░░██░██░██░██  ░██  ░█░░░░░█ 
░██   ░    ░██░██░██   ░░████░██░░██ ░█   ░█    ░█ 
░██        ░██░██░██    ░░███░██ ░░██░ ████     ░█ 
░░         ░░ ░░ ░░      ░░░ ░░   ░░  ░░░░      ░ 
```

```text
symbol:  ←→↓↑⇄⇋ *∗†‡⁂
greek:    αβγδΔεκλμσπτρφχ
logic:  ∴ ∵
math: − ∓ ∙ √ ∛ ∜ ∞ ∟ ⊾ ∠ ∡ ⊾ ≈ ≙ ≡ ≤ ≥ ∅
```

## MINK04 -- Neurorevalidatie ##

### glossary ###

|    abbr   |                            def                            |
|-----------|-----------------------------------------------------------|
| FSHD      | fascio-scapulo-humeral dystrophy (= M. Landouzy-Dejerine) |
| NA        | neuralgic amyotrophy                                      |
| UDO       | urodynamisch onderzoek (_nl_)                             |
| BPN[^BPN] | brachial plexus neuropathy                                |
| NMA       | neuromusculaire aandoeningen (_nl_)                       |
| DMD       | Duchenne Muscular Dystrophy                               |
| MRC       | Oxford Medical Research Council                           |
| RA        | revalidatiearts (_nl_) ; hier NIET: rheumatoid arthritis  |
| ICF       | International Classification of Functioning               |
| ICF-CY    | ICF Children and Young adults                             |
| CP        | cerebral palsy                                            |
| CVA       | cerebrovascular accident                                  |
| iCVA      | ischaemic CVA                                             |
| hCVA      | haemorrhagic CVA                                          |
| ICH       | intracerebral haematoma                                   |

|    abbr   |                 def                 |
|-----------|-------------------------------------|
| Ax        | anamnesis                           |
| Aex       | aethiology                          |
| Dx        | diagnosis                           |
| DDx       | diff diagnosis                      |
| Epix      | epidemiology                        |
| Fn(x)     | function(s)                         |
| Sx        | symptoms                            |
| Tx        | treatment                           |
| S. <name> | <name> syndrome, Syndrome of <name> |
| M. <name> | morbus <name> , <name> disease      |
| M. <anat> | musculus (muscle)                   |
| Pl.<neur> | plexus                              |
| Tr.       | tract(us)                           |

## MINK04-w01 ##

<!-- [dir:MINK04-w01](./w01/) -->

### HC0 Intro,ICF,Historie

#### Inhoud minor

- groei en degeneratie
- motor learning
- neuroplasticity
- paramed
    + oefenTx
    + zelf-management
    + motivational interviewing  
- med-tech interventies
    + labs
    + electrostimulation (m.n. voor CNS-aandoeningen; perifere zenuwen zijn dan gemakkelijk(er) individueel te stimuleren)
- research project
    + PICO met research proposal
- caput selectum
    + mitochondrial disease

#### Modelaandoeningen ####

- volw.
    + CVA
    + Dwarslasesie
    + FSHD
- kindRooster Keuzemodule Q8 - Bewegingsanalyse en klinisch redeneren bij patiënten met loopproblemen
    + CP
    + Spina bifida
    + M. Duchenne

#### Toetsing ####

|                         vorm                        | t(#w) |  %  |
|-----------------------------------------------------|-------|-----|
| schriftelijk (theorie, open vragen, indiv)          | 5     | 35  |
| schriftelijk (probleemanalyse en plan, open, indiv) | 10    | 35  |
| wetenschappelijk verslag (groep)                    | 9--10 | 20  |
| presentatie vh verslag (groep)                      | 9--10 | 10  |
| toetsing KVH (indiv)                                | 5     | O/V |
| formatieve toetsen                                  | w3+8  |     |


Eind van elke week: reflectie (per 2 pers.)

- WGs zijn (obv) ook tentamenstof
    + idem met voorbereidende ZSOs

#### ICF domeinen ####

[ICF-CY-NL](https://class.who-fic.nl/browser.aspx?scheme=ICF-CY-nl.cla)

- functies en anatomische eigenschappen
    + Specifieke werking van een weefsel, orgaan, orgaanstelsel of van organen uit meerdere orgaanstelsels tezamen, welke bij geboorte aanwezig is of daarna, voornamelijk op grond van rijping ontstaat.
- activiteiten
    + Concrete activiteit die plaatsvindt in een omgeving, waarbij verschillende functies zodanig worden gecombineerd dat de activiteit meer is dan de som van de delen.
        * capacity ('kan iemand een activiteit')
        * performance ('doet iemand een activiteit')
            - e.g. bij somatoforme aandoeningen (SOLK, conversie, disuse (ookwel 'developmental disregard' in de kinderreva)
- participatie
    + Participatie is de aard en de mate van deelname en betrokkenheid van een individu aan het maatschappelijk leven (sociale rolvervulling)
        * learning and applying knowledge
        * general tasks and demands
        * communication
        * mobility
        * self care
        * domestic life
        * interpersonal interactions and relationships
        * major life areas
        * community, social and civic life
- environment
    + internal/person
        * gender
        * age
        * comorbidity
        * physical endurance
        * coping style
        * social background
        * education
        * profession
        * past experiences
        * character style/type (i.e. motivation)
    + external/environment
        * products
        * close milieu (family)
        * SES (socio-economic situation)
        * institutions
        * social norms
        * culture
        * built-environment
        * political factors
        * nature


#### ICF ####

- [ICF online browser](http://apps.who.int/classifications/icfbrowser/)

![ICF-extra](./src/ICF-extra.png)  
ad. ENVIRONMENTAL/Internal = persoonlijke factoren  
ad. ENVIRONMENTAL/External = omgevingsfactoren  
ad. zoek de termen hieronder in fig. hierboven voor het gebruik

| professionele termen/negatief jargon |
|--------------------------------------|
| impairments                          |
| activity limitations                 |
| participation restrictions           |
| (disability, overkoepelende term)    |
| ~~handicap~~                         |

#### ICF structure ####

![ICF-structure](./src/ICF-structure.png)

- **ICF**
    + b BODY FUNCTIONS
        * spierfunctie
        * ..
    + d ACTIVITIES AND PARTICIPATION
        * concrete activiteiten die plaatsvindt in een omgeving, waarbij versch. functies zodanis worden gecombineerd dat de activiteit meer is dan de som der delen. (bijv: lopen is een activiteit, waar je o.a. spierfunctie, evenwicht, coordinatie etc voor nodig heb)
            - capacity ('kan iemand een activiteit')
            - performance ('doet iemand een activiteit')
                + e.g. somatoforme aandoeningen (SOLK, conversie, 'disuse' (bij kinderen: 'develepmental disregard'))
        * Participatie is de aard en mate van deelname en betrokkenheid van een indiv aan het maatschappelijk leven (soc. rolvervulling)
            - learning and applying knowledge
            - general tasks and demands
            - communication
            - mobility
            - self care
            - domestic life
            - interpersonal interactions and relationships
            - major life areas
            - community, social and civic life
    + e ENVIRONMENTAL FACTORS
        * internal/person
            - gender
            - age
            - comorbidity
            - physical endurance
            - coping style
            - social background
            - education
            - profession
            - past experiences
            - character style/type (i.e. motivation)
        * external/environment
            - products
            - close milieu (family)
            - SES (socio-economic situation)
            - institutions
            - social norms
            - culture
            - built-environment
            - political factors
            - nature
    + s BODY STRUCTURES
    	* structures of the nervous system
    	* eye, ear, related
    	* voice/speech structures
    	cardiovascular, immunologic
    	* digestive, metabolic, endocrine systems
    	* genitourinary, reproductive systems
    	* movement-related structures

#### Geneeskunde vs. Revalidatiegeneeskunde flowchart ####

![gnk-reva-flowchart](./src/gnk-reva-flowchart.png)

#### Historie ####

- Johanna-Stichting (tehuis voor gebrekkige en mismaakte mensen)
- ~1899--1900
- eerste initiatief tot participatie-gerichte zorginstelling
- kinderrevalidatie is historisch de initiele focus geweest
- volw. later in beeld gekomen
    + WWII
        * oorlogsslachtoffers
        * verzorginsstaat
        * engels voorbeeld
        * 'rehabilitation
        * ..
- ziekenhuisrevalidatie
    + 1958: eerste migratie(s) van revalidatiecentra naar ziekenhuizen (1st: Revalidatiecentrum Tolbrug (JBZ))
- Onderwijs
    + 50s: mytyl- (lichamelijke beperking) en tyltylscholen (meerv. beperking)
- Woning
    + 1962 Stichting Het Dorp: eerste (zelfstandige) woonvorm (Arnhem)
    + later landelijke woonvormen en grootschalig=>kleinschalig
    + 'focus-woningen'

#### Traditionele doelgroepen ####

- pre-WWII (kinderen)
    + CP (cerebral palsy)
    + poliomyelitis
    + spina bifida
    + congenitale (ledemaat)afw
- post-WWII (volw)
    + amputaties
    + traumaslachtoffers
- 70s:
    + verworven hersenletsel (CVA, hersentrauma), 
    + ruggenergletsel (dwarslaesie)
- 80s:
    + neuromusculaire aandoeningen
- 90s: 
    + pijnsyndrommen, 
    + cognitieve en gedragsmatige revalidatie
- \>00: 
    + neurodegeneratieve aandoeningen

#### Nieuwe doelgroepen ####

- post-IC revalidatie
- metabole aandoeningen
- oncologische aandoeningen
- geriatrische revalidatie

#### Interdisciplinair ####

- revalidatiearts
- paramedici (fysio / ergo/ logo)
- perimedici (psycholoog / maatschappelijk werker)
- technici (intrstrumentmaker / schoenmaker)

#### Veelzijdigheid ####

- Diagnostic options
    + motor Dx
    + instrumentele motor Dx (lab)
    + spraak-taal Dx
    + cognitieve Dx
    + Gedragsobservatie (ADL)
- Tx options
    + vaardigheidstraining
    + prothesiologie/orthesiologie
    + electrostim (TES, FES)
    + hulpmiddelen / aanpassingen
    + gedragsadvies / leefregels
    + zenuw-, spier-, klierblokkades
    + functionele chirurgie
    + PharmacoTx (incl. baclofen intrathecaal)
    + cognitieve training
    + gedragsTx


### HC1 CVA ###

#### Epix ####

|   (in NL)    |      prvl      | incd (cases/y) |
|--------------|----------------|----------------|
| M. Parkinson | 25000          |           5000 |
| MS           | 15000          |           1500 |
| CVA          | 450000 (~3.3%) |          55000 |

|      Risk      |  %  |
|----------------|-----|
| CVA-risk >55yo | 21% |
| CVA-risk <65   | 10% |
| Male > Female  |     |

|    Types     |  %  |
|--------------|-----|
| ischaemic    | 80% |
| haemorrhagic | 20% |

|         Subtypes/loc        |  %  |
|-----------------------------|-----|
| A. cerebri med (MCA, _en_)  | 51% |
| small vessel                | 13% |
| brainstem                   | 11% |
| >1 territory                | 9%  |
| A. cerebri ant (ACA, _en_)  | 5%  |
| A. cerebri post (PCA, _en_) | 7%  |
| Cerebellum                  | 4%  |


![CVA-heatmap](./src/CVA-heatmap.png)

AChA = anterior choroidal artery = A. chorioidea anterior  
PICA = posterior inferior cerebellar artery = A. cerebelli inferior posterior  
AICA = anterior inferior cerebellar artery = A. cerebelli inferior anterior  
SCA = superior cerebellar artery = A. cerebelli superior  
LSA = lenticulostriate arteries = Aa. centrales anterolaterales  

![CVA-types-overview](./src/CVA-types-overview.png)

![CVA-types-overview2](./src/CVA-types-overview2.png)

> **Maligne ACM infarct**
> Jaarlijks worden circa 27.000 mensen in Nederland getroffen door een cerebrovasculair accident (CVA); voor 85 betreft het een herseninfarct. Een klein deel van hen krijgt een infarct in het volledige stroomgebied van de A. cerebri media en eventueel de A. cerebri posterior en/of de A. cerebri anterior. Achteruitgang van de toestand kan optreden ten gevolge van cytotoxisch oedeem, dat een ruimte-innemende werking heeft.1 2 Deze oedeemfase treedt meestal op tussen de 2e en 5e dag en wordt gekenmerkt door progressieve bewustzijnsdaling gevolgd door tekenen van transtentoriële herniatie van de uncus, zoals een ipsilaterale wijde lichtstijve pupil, decorticatie- of decerebratiehouding en andere tekenen van hersenstamdisfunctie.1 De sterfte in deze oedeemfase is hoog, ook als intracraniële drukverlagende behandelingen worden toegepast.1 3 4 Vaak betreft het relatief jonge patiënten, jonger dan 60 jaar.1 2

> Dit type infarct wordt ook wel het ‘maligne A.-cerebri-media-infarct’ genoemd vanwege de infauste prognose.1 Een therapie waarvan het effect is bewezen is niet voorhanden. Hemicraniëctomie is een methode waarmee verdere secundaire schade kan worden beperkt door de ruimtebehoefte van het infarct naar extracranieel te geleiden. Op die manier wordt transtentoriële herniatie voorkomen.

source: [ntvg](https://www.ntvg.nl/artikelen/hemicrani%C3%ABctomie-als-behandeling-bij-3-pati%C3%ABnten-met-een-maligne-cerebri-media-infarct/volledig)

#### RF ####

- Infarction
    + same RFs as atherosclerosis
        * DM
        * hypertension
        * smoking
        * hyper[Chol]
    + cardial emboli
        * Afib
        * kleplijden
    + vascular
        * angiodysplasia
        * (lamina) intima rupture
- Haemorrhagic
    + hypertension
    + AV malformation

#### Motor cortices ####

![CVA-motor-cortex](./src/CVA-motor-cortex.png)  
ad. PMC = primary motor cortex = M1  
ad. PMA = premotor area  
ad. SMA = supplementary motor area  
ad. PMA+SMA = secondary motor cortices  

#### Sx ####

|         Sx         |         S. UMN        |        S. LMN       |
|--------------------|-----------------------|---------------------|
| paralysis type     | spastic paresis       | flaccid paralysis   |
| tonus              | hypertonia            | --                  |
| atrophy            | disuse                | severe              |
| reflex             | hyperreflexia         | hypo-/areflexia     |
| path. reflex       | Babinski sign         | --                  |
| fascicul./fibrill. | --                    | possible            |
|                    | contralateral paresis | ipsilateral paresis |

pyramidal + parapyramidal = S. UMN

#### Sx sensorimotor ####

- paresis
- loss of motor selectivity[^8]
- loss of sensibility
- hypertonia (spasticity)
- coordination deficits, slowness


[^8]: complex motor execution (i.e. walking) is changed => pts. fall back on more primary motor execution programs ( these are (evolutionarily): `flexion in arms + extension in legs => gait asymmetry` )

![CVA-spastic-hemiparesis](./src/CVA-spastic-hemiparesis.png)  
ad. key problem of spastic hemiparesis is a disturbed _selective activation_ of (predominantly contralateral muscles, often complicated by _supraspinal disinhibition of spinal reflexes_ (=> causes hyperreflexia))

![CVA-spastic-paresis](./src/CVA-spastic-paresis.png)


#### Sx spastic paresis ####

- pos. Sx:
    + proprioceptive reflexes: hyperreflexia, clonus, hypertonia
    + exteroceptive reflexes: spasms, withdrawal response, positive support action
    + abnormal efference: associated reaction, co-contraction, areflexia, spastic dystonia
- neg. Sx:
    + muscle weakness
    + loss of motor selectivity
    + inc. muscle fatigue
- secondary (non-neural) Sx:
    + muscle stiffness
    + muscle contracture
- 'non-visible' Sx:
    + dysphagia
    + dysarthria
    + aphasia
        * sensor (Wernicke)
        * motor (Broca)
    + cognitive impairments
        * amnesia (memory/recall involves: frontal cortex AND cerebellum)
        * dec. concentration/vigilance
        * bradyphrenia + neglect
- misc Sx
    + bradykinaesia
    + behavioural problems ((pre)frontal cortex)
    + visual impairments (occipital cortex)
    + integration of hearing, memory and language (temporal cortex)
    + movement finetuning AND impulse control (basal ganglia)

#### Predictors of ADL-recovery ####

- **initial ADL score**
- trunk balance
- urine incontinence
- age
- CVA in Hx
- comorbidity
 
#### NOT related to ADL-recovery ####

- sex
- ethnicity
- RFs for CVA
- affected hemisphere
- CVA volume (locatie vd laesie is bepalender voor het functieverlies dan het volume vd laesie)




#### Upper extremity recovery ####

- affected arm
    + normal/functional (20%)
    + severe (27%)
        * no recovery (66%)
        * partial recovery (20%)
        * full recovery (5--18%)
    + moderate (53%)
        * full recovery (50%)
        * partial recovery (50%)

#### Post-CVA prediction of dexterity ####

- Scoring: Fugl Meyer Arm (FMA) / Fugl Meyer Hand (FMH?)
    + FMA arm-hand ≥13 @ 3w post-CVA => PPV 0.95--1.00
    + most critical/essential:
        * (residual) wrist ext
        * (residual) finger ext

#### Gait disability after stroke ####

- causative factors
    + motor deficits affected leg
    + sensory deficits affected leg
    + balance deficits
- **!! Standing balance the most important determinant of gait independence !!**

ad. FAC = Functional Ambulatory Capacity   

#### Tx for improving leg motor control ####

- soft-tissue surgery (e.g. SPLATT)
- functional electrostimulation (e.g. N. peroneus)
- spasmolysis (e.g. BTX-A (botulinum toxin A))

_ _ _ _

### HC2 CP ###

#### Definition ####

- CP criteria
    + brain damage
    + w/ motor Sx
        * neural Sx are primary and non-progressive
        * orthopaedic Sx are secundary and progressive
        * ( ~~stationaire ziekte~~ => docent had moeten zeggen 'statische' ziekte, if anything, maargoed )
    + AoO <1yo

#### Epix ####

|          | val (kinderen in NL) |
|----------|----------------------|
| prvl     | 1:500 = 0.2%         |
| pop.tot. | 2.5M                 |

#### Aex ####

- causative mechaninisms
    - hypoxia/ischaemia
        + [Periventricular leukomalacia (PVL)](https://en.wikipedia.org/wiki/Periventricular_leukomalacia) => prematuur geboren
        + basal ganglia necrosis => a terme geboren
        + infarction
    - pharmacologisch
    - toxisch
    - traumatisch (baby laten vallen)
        + incl. kindermishandeling
        + ongevallen pre-partum
    - congenitaal/aanleg
    - infectieus (zowel tijdens als na bevalling)
- cause-time relation
    + \~75% pre-partum
    + \~5--10% tijdens 
    + \~15% post-partum

> - bij a terme babies geeft hypoxia m.n. schade aan de basale kernen (=> daar loopt de Tr. cort.spin doorheen, dus ontstaat er een gemengd dyskinetisch syndroom; CAVE: slapte in nek/rug-spieren)
> - bij pre-terme babies geeft hypoxia juist wittestof schade

- Subtypes
    + ~85% spastic
    + ~1% dyskinetic
    + <5% ataxic
    + ook mengtypen bestaan
        * ook met chorea/athetose (basale ganglia damage)
        * basale ganglia damage komt voort uit hypoxie in de a terme periode
    + mengvorm dyskinetic/spastic
        * geeft vaak slapte vd nek en rug

- MRI
    + T1 = anatomy
    + T2 = pathology
        * FLAIR = T2 + liquor herkleurt naar zwart

#### Spastic CP ####

- Sx
    + hypertonia
    + spasticity
    + vaardigheidsverlies
    + persistent fist clenching
    + 'vleugelen' arm
    + circumductio
    + scharen vd benen
    + hyperreflexia + path. reflexen
- Tx (voor spasticiteit algemeen)
    + FysioTx
    + Orthopaed. hulpmiddelen
    + BTX-A spasmolysis
    + baclofen
        * per os
        * intrathecaal (baclofenpomp)
    + neurolysis
    + chirurgieTx

#### Dyskinetic CP ####

- Sx
    + afw. stand ledematen door aanspannen van agonist- en antagonist-spieren
    + overtollige bewegingen

#### Ataxic CP ####

- Sx
    + hypotonia
    + breedbasisch looppatroon
    + ongelijke paslengte
    + dysmetria
    + intentietremor
    + nystagmus

#### Misc (non-motor) Sx ####

- Visusproblemen
    + 10% ernstige visusproblemen
    + 40% milde visusproblemen
    + exo/esotropie (scheelzien)
    + retinopathie van de prematuur (ROP)
    + cataract bij TORCHES (TO=toxoplasma,R=Rubella,C=CMV,HE=herpes,S=syfilis,Z=zika)
    + cerebrale vissustoornis (kan bij alle CPs voorkomen)
        * hemianopsie
- Gehoorproblemen
    + 5--7% vd kinderen (m.n. ook bij PVL (periventriculaire leucomalacia))
- Voedingsproblemen
    + kauwen
    + slikken
    + aspiratie
    + reflux
    + kwijlen
    + obstipatie
- Ondervoeding
    + 27% van alle kinderen
        * 1/3 unilateraal
        * 2/3 bilaterale spastische parese
    + oorzaak
        * mondmotoriek probleem
        * comm. probleem
        * hoger energieverbruik door spasticiteit en dystonia
    + Tx:
        * PEG-sonde: voedingssonde via de buikwand naar de maag
- S. West
    + salaamkrampen-epilepsie
- Epilepsie
    + avg. 40% vd kinderen
    + vaak focale, non-motor aanvallen (staren)
    + samenhangend met parese-ernst
        * 50--75% pts. met tetraparese
        * 40--50% pts. met hemiparese
        * 15--25% pts. met diplegia/dyskinaesia
    + occurrence: 70-80% <7yo
        * tetraparese vaak rond 1e jaar
        * diplegie/hemiparese rond 3--4yo
    + complex partiële aanvallen = focale niet-motor aanvallen (NIEUWE TERMINOLOGIE)
    + moeilijk te herkennen voor ouders/naasten/verzorgers
        + `CAVE`: word ook door artsen vaak aangezien voor andere (non-epilepsie) problematiek
- Cognitieve problemen (33% vd kinderen)
    + 30% psychomotore retardatie
    + `CAVE`: voor veel testen is een goede handfunctie nodig
    + onder- en overschatting
- Gedragsproblemen
    + achterblijven social-emotionele ontwikkeling
    + veel lichamelijkheid
    + lager IQ
    + trage info-verwerking
    + beperkte comm.mogelijkheden
    + zelfvertrouwen
    + overvragen
    + ondervragen
    + te veel prikkels
- Scoliosis
    + 5--64% kinderen met CP

### WG2 CVA/CP ###

#### Casus 1

- Hulpvraag: kom ik in aanmerking voor electrostim? (zodat ik beter kan lopen)
- Dx: CVA, herseninfarct R hemisfeer Arteria media cerebri R
- neven-Dx: DM, neuropathie, AV blok (waarvoor pacemaker)
- Fnx-stoornis: verminderde spierkracht, ( (hemi)neglect ), ~~loop-onzekerheid~~, (AV-block), hemi-parese L (distaal>proximaal), verstoorde reflexen, evt. milde spieratrofie
- beperkingen activiteit: mobiliteit, niet meer cafe, geen ~~autorijden~~ (= soc. apartc), niet meer vissen, armactiviteit
- soc. partc.: verminderd door beperkte mobiliteit, autoreiden, sociale interacties, vissen, biljarten
- pers. factoren: geen rollator willen, loop-onzekerheid, cognities (evt. depressie)
- omgevingsfact: klein dorp naast Eindhoven, geen goede infrastructuur (geen OV), kleine sociale kring (en die ook kwijtgeraakt)
- prognose mobil: onveranderd is prognose laag, maar kan veranderen met zijn motivatie. Risico op: valgevaar!, totaal isolement
- prognose soc.prtc: ''
- probleemanalyse: enkelinstabiliteit, mobiliteit, ~~OV~~, cognities/motivatie?
- ==> operatief de enkel stabiliseren wordt ook gedaan.
- enkel-voetorthese: voor zijn klappende enkel goed, nadeel is dat zijn enkel/voet stijf zijn in de orthese. ==> deze man heeft een stijve orthese gekregen (foutief) en zou een 'dynamische orthese' (met scharnier) moeten hebben gekregen.
- niet-slagen electrostim.: (perifere) polyneuropathie (hiervoor heb je wel intacte perifere zenuwen nodig, wil het mogelijk zijn/werken), CVA met dood hersenweefsel
- pacemaker spelbreker: pacemaker-activiteit kan electrostim. verstoren
    + ==> is eigenlijk helemaal geen contra-indicatie
- advies: mobiliteit, motivatie    
- korte termijn: **dynamische orthese**, cognitieve gedragstherapie?, 
- lange termijn: **operatie tbv enkelstabiliteit**, ~~fysio/ergo?~~
- terecht 5y geen auto rijden: nee, Pt. niet nagevraagd en arts niet uitgelegd t.z.t.
- arts ongemakkelijk: ^ hierom, omdat de arts het niet initieel verteld dat hij wel gewoon autorijden (meestal komt dit omdat in het ziekenhuis door de neuroloog wordt gezegd: "je mag een jaar lang geen autorijden"=> pt. houden dit vol/blijven hierin hangen)
- RA: informeren, indicaties stellen, Tx bepalen if needed 

#### Casus 2

- Dx: CP
    + ja, ontstaan <1yo
- nevenDx: mentale retardatie (IQ+sociaal+emotioneel), heupdysplasie
- Fnxstoornissen: ADL-afhankelijk (zelfzorg), kan niet lopen, geen sta/transfer-mogelijkheid, niet kunnen zitten zonder ondersteunig, fijne motoriek is beperkt en vertraagd, pathologische reflexen L>R, (nagenoeg) geen L-handfunctie, moeite/traagheid bij eten, ~~autonome stoornissen~~ kwijlen is meestal door verminderd slikken (niet door verhoogde aanmaak), neemt een gedeelte van de omgeving niet waar, spitsvoet, begrip/comm.stoornissen
    + welke Fnxstoornissen passen er nog meer bij?
        * epilepsie
        * scoliose
- activiteiten: kan niet meer zitten, kan niet staan, gehele dag begeleiding nodig, naar school gaan (gaat nu naar mytylschool, ZMLK leerlijn (Zeer Moeilijk Lere)), kan niet meer praten (gebruikt PECS),
- GMFCS = niv. 4 (=> 5) 
- MACS => wordt per arm gegradeerd!
    + links: niv. 4
    + rechts: niv. 2
- CFCS
    +  niv. 4-5
-  EDACS
    +  ~~niv. III~~, niv. II-III-IV

_ _ _ _

### HC3/ZSO Ruggenmergletsel (SCI) ###

| abbr |        def         |
|------|--------------------|
| SCI  | spinal cord injury |

- voor elke dwarslaesie heb je **op** het niveau vd laesie een **perifere laesie**(\*) (LMN syndrome) ; **onder** het niveau vd laesie is er een **centrale laesie** met spasticiteit (UMN syndrome)

(\*) dit is (functioneel) een perifere laesie, vanwege destructie vh α-motorneuron

- hoge druk blaas:
    + ontkoppeling vd samenwerking tussen M. Detrusor en de blaassphincters

- Scoren voor Px post-SCI (?)
    + age (>65)
    + M. quadriceps kracht
    + M. gastrocnemius kracht
    + L3
    + S1
    + 'total score = ( points / 40 )'

#### Barthel index

| score | Barthel-index: ADL-assessment |
|-------|-------------------------------|
|     1 | unable                        |
|     2 | attemps but unsafe            |
|     3 | moderate help                 |
|     4 | minimal help                  |
|     5 | normal                        |

#### Spina bifida

- 'Spina bifida' kan je beschouwen als een 'aangeboren dwarslaesie'

- dwarslaesie:
	- cerebrale aanlegstoornis (invloed op planning etc.)
	- sterk hulpafh.
	- vaak rugproblemen
	- tethered cord = ruggenmerg vastgegroeid aan de onderkant; schade door rek

- Types
    + Occulta
        * asymptomatic; kleine opening ruggenmerg, geen protrusie
    + Meningocele
        * asymptomatic; rarest, meningeal herniation, no protrusion spinal cord
    + Myelomingocele
        * most severe; in 3rd week, wervels sluiten niet => wel protrusie vh ruggenmerg
- Sx
    + spasticity
    + hyperreflexia
    + clonus
    + Babinski
    + fatigue
- Tx
    + phenolisation/fenolisatie = inject phenol to induce myelinolysis of affected nerves, counteracting spasms
- Acute phase miction
    + de blaas is hypocontractiel voor 6--12w => katheterisatie nodig
        * later otnwikkeld zich een spastische blaas
        * Tx van UTIs: alleen bij symptomatisch lijden
- defaecatie
    + bedfase: laxeren m.b.v. bisacodyl
    + mobilisatiefase: micro-enema (MICROLAX©): induceert defaecatie bij inbrengen in de anus
    + inco-materiaal
- anticonceptie
    + spiraal heeft de voorkeur, vanwege het verhoogde risico op thrombosis bij dwarslaesie (+ verhoogd thrombosis-risico door OAC)
- erectie
    + hoge laesie: reflexerectie
    + lage laesie: pyschogene erectie
    + Tx:
        * viagra per os
        * intracaverneuze injectie
- ejaculatie
    + Tx:
        * vibrator
        * electrostim
        * chirurgieTx
- autonoom zenuwstelsel
    + hyperstimulatie sympathicus **onder niveau vd laesie**
    + hypostimulatie vd parasympathicus
- Laesie <T6 (T6 of (anatomisch) hoger)
    + leidt tot vasocon. in benen en dus HT
    + kan verder dysreguleren via hoge prikkels
- neuropatische pijn
    + Tx:
        * PharmacoTx (amitriptyline, gabapentine, carbamazepine, lyrica)
        * TENS
        * psychologisch ondersteuning in de acceptatie
- decubitus
    - inco bestrijden
    - eiwitten suppleren
    - drukvermindering
    - Tx:
        + chirurgieTx, verwijdering vd laesie; daarna bedrust
        + 4w rust
        + 2e chirurgieTx om huidbedekking aan te brengen
        + 6w rust

_ _ _ _

#### > > > move to FnO !! ####

| score |                            Brooke armfunctie                             |
|-------|--------------------------------------------------------------------------|
|     1 | De armen langs de zijde, de patient abduceert armen in volledig aanraken |
|     2 | Armen kunnen boven hoofd alleen met flexie elleboog of bijspieren        |
|     3 | Handen kunnen niet boven het hoofd, maar kan een vol glas naar mond      |
|     4 | Kan de handen naar de mond maar zonder glas                              |
|     5 | Kunnen handen niet naar de mond maar kan wel een pen oppakken            |
|     6 | Handen kunnen niet naar de mond en geen gebruiksfunctie van handen       |

### WG3 Ruggenmergletsel ###

#### Casus 1 ####

##### Part 1 #####

- Dx?
    + Partiële dwarslaesio (met paraparese)
- nevenDx?
    + myelumcompressie
    + wervelFx
    + ribFx
    + pneumothorax
- Fnx-stoornissen?
    + partiele paraparese
    + verlies spierkracht (heupflexio, knie-extensio, meer distaal is motoriek afwezig)
    + verlies blaasfunctie (atonia in M. detrusor en sphincter)
    + stoornis in defaecatie (atonia in darm)
    + verlies sensibiliteit onder niveau lies
        * verlies sensibiliteit rijbroekgebied (S4--S5)
    + erectiestoornis, ejaculatiestoornis
- activity impairments?
    + kan niet meer lopen, verminderd kunnen zitten, kan geen transfers meer maken, incontinentie voor urine en feces (=> toiletgang is gestoord), kan niet meer traplopen, kan niet meer sporten, seksuele activiteit.
- soc.ptc.?
    + motorrijden, beroepsbeoefening (monteur in tech. dienst van fabriek), voetballen
- personal factors?
    + sporten/sportief (voetbal), samenwonend (hij heeft een partner), leeftijd 59yo, voorheen gezond
- environmental factors?
    + Fysiek werk, trap in de woning, infrastructuur door wonen in dorp, gezin/familie
- long-term Px for mobility
    + Functie kan post-op nog verbeteren, maar geen garantie kan worden geboden voor herstel van functies. Wel waarschijnlijk dat hij verbeterd in de aankomende tijd. Volledig herstel mobiliteit lijkt onwaarschijnlijk (afh. van aard myelumschade/verloop herstel).
    + Als dit niveau van functioneren nog zou bestaan >3w, dan wordt verder functieherstel (erg) onwaarschijnlijk.
- long-term Px for autonomic functions?
    + m.n. blaascontrole/feces-incontinentie van belang. Verbetering is NIET waarschijnlijk. ~~mogelijk, evt. NIET te verwachten voor blaasfunctie.~~
    + `CAVE:` ontstaan hogedrukblaas is een veel voorkomende complicatie.
- long-term Px for ADL?
    + Mobiliteit zonder hulpmiddelen is onwaarschijnlijk. ADL zonder externe assistentie (mantelzorg) is misschien mogelijk.
    + ADL-herstel moet waarschijnlijk m.b.v. inco-materiaal, en aanpassingen in huis. Nood tot persoonlijke assistentie initieel onvermijdelijk.
    + _met deze vraag wordt bedoeld_: basis ADL: gezicht wassen, aankleden, etc.
    + In de praktijk is ADL-afhankelijkheid niet aan de orde bij thoracale en zelfs niet bij de meeste cervicale laesies. M.n. bij hoge cervicale laesies (C5 of hoger) is ADL-afhankelijkheid min of meer gegarandeerd.
- long-term Px for soc.ptc.?
    + Ptc. voetbal, motorrijden, beroepsbeoefingen behoeven aanpassing t.b.v. capaciteitsverlies. Rolverandering bij deze activiteiten is onvermijdelijk.
- common complications?
    + DVT (`CAVE:` ontstolling wordt alleen gegeven in de acute fase post-trauma, na mobilisatie wordt ontstolling gestaakt ivm (lange-termijn) risico's hiervan), decubitus, atrofie, psychosociaal, UTI, dys/paraesthesia, pijn door spasticiteit/spastische spieren, osteopenia/osteoporosis.
- initieel revalidatiebeleid / Tx ?
    + in het begin is relatief snel herstel waarschijnlijk, maar later zal herstel langzamer verlopen en de progessie hierin tot een halt komen. Volledig herstel is waarschijnlijk niet mogelijk.
    + Gerichte fysiotherapie/training (+ opname: team vormen voor remobilisatie, urinekatheter, darmbeleid voeren m.b.v. zetpillen), evt. pijnmedicatie, rolstoel / mobility hulpmiddelen, lotgenoten/patientengroep benaderen, (psycho-educatie).
    + avg. gaan pt. na 2w naar het revalidatiecentrum.

##### Part 2 #####

- Hulpvraag van pt.?
    + advies over voortzetting werk
    + kan ik nog motorrijden?
    + hoe kan ik mijn loopvaardigheid verbeteren?
        * wat is het te verwachten eindniveau?
    + is een exoskelet of stamcelTx zinvol/mogelijk?
- Advies t.a.v. voorzieningen (traplift, rolstoel, etc.)
    + traplift
- Nieuwe Px qua mobility/ADL/soc.ptc?
    + 
- Wat is uw belangrijkste insteek als het gaat om pt.'s mobiliteitsvraag?
    + 
- Welke Tx adviseert u en welke orthese gaat u evt. adviseren?
- Advies t.a.v. stamcelTx

- - - -

### MRC + ASIA ###

|   MRC   |              Muscle Fn               |   ASIA  |                                   Impairment Scale for classifying spinal cord injury                                    |
|---------|--------------------------------------|---------|--------------------------------------------------------------------------------------------------------------------------|
| **gr.** | **Fn**                               | **gr.** | **desc**                                                                                                                 |
| 0       | No contraction                       | A       | **Complete injury.** No motor/sensor Fn is preserved in S4 or S5                                                         |
| 1       | Muscle flickers                      | B       | **Sens incomplete.** Sensory, but not motor Fn is preserved below injury-lvl, incl. sacral segments                      |
| 2       | Full range of motion, grav elim.     | C       | **Motor incomplete.** Motor Fn is preserved below injury-lvl, >50% of muscles tested below injury-lvl have MRC-grade <3. |
| 3       | Full range of motion, against grav   | D       | **Motor incomplete.** Motor Fn is preserved below injury-lvl, ≥50% of muscles tested below injury-lvl have MRC-grade ≥3  |
| 4       | Full range of motion, against resist | E       | **Normal.** No motor or sensory deficits, but deficits existed in the past.                                              |
| 5       | Normal strength                      |         |                                                                                                                          |

- ASIA: What is tested?
    + motor level of arms and legs
    + nociception of entire body
    + tastzin of entire body
    + tonus/spierspanning vd sphincter ani
    + anal sensibility (present/absent)

#### SCI (cont) ####

- Acute fase controles
   + From occiput to coccyx
        * Soft tissue swelling/bruising
        * Point of spinal tenderness
        * Gap
        * Spasm of associated muscles
   + Neurological exam using ASIA
- ASIA             Neurological exam for SCI
    + Neurological level -> lowest segment where motor and sensory function remain normal
- Neurogenic shock = Hypotension due to sympathetic denervation of the heart through T1-T4
    + Warm skin, normal urine, bradycardia
- Leasion T6 => Disrupted autonomic nervous system, leads to hypothermia 
    + Also leads to autonomic dysreflexia, respiratory problems and hypotension
- NEXUS = X-ray if pain/tenderness in posterior midline, neurological deficit, alertness 
- MRI = Always with neurological abnormalities

- Pressure ulcers
    + Localized over bony points
    + Tx:
        * Use pressure reducing mattresses
        * Keep dry to avoid pressure elevation 

| score |               Decubitus                |
|-------|----------------------------------------|
|     1 | non-blanchable redness                 |
|     2 | partial loss of thickness/blisters     |
|     3 | full loss (visible fat)                |
|     4 | full tissue loss (visible muscle/bone) |

Bladder complications     50% to 80%
    • Reflexic type        Both bladder as well as the sphincter spastic
    • Areflexic type        Stress incontinence
    • UTI            Increased spasticity, worsening incontinence, fever/malaise
Autonomic dysreflexia    Due to overprikkeling
    • Rapid onset hypertension, severe headache, sweating, bradycardia
    • Make legs dangle and treat the cause
DVT            In 50% of patients without prophylaxic agents
Respiratory issues    84% in C1-C4 injuries; above C5/AIS-A 95% mechanical ventilation
Spasticity        Can be caused by nociceptive sauce
Pain             Prevalent in 80% of cases; opioids for moderate to severe pain 
Heterotopic ossification: Bonification of soft tissues
    • Treat with non-steroidal anti-inflammatory drugs and later surgery
Post-traumatic Syrinjgomyelia -> intermedullary cavity after SCI
    • Pain, weakness, altered bladder/autonomic system 
_ _ _ _

## MINK04-w02 ##

[dir:MINK04-w02](./w02/)

### ZSO Neuromusculaire aandoeningen (NMA) ###

#### Part 1 ####

1. welke 3 redenen worden aangegeven voor wijzigingen in de 2018 update vd DMD-GL vs. DMD-GL van 2010?
    + address the needs of patients with prolonged survival
    + provide guidance on advances in assessments and interventions
    + consider the implications of emerging genetic and molecular therapies for DMD
2. Hoe wordt de diagnose gesteld? Wat zijn de vorege symptomen van DMD?
    + Initial Dx begins (typically in early childhood) after suggestive signs and symptoms:
        * weakness
        * clumsiness
        * Gowers' sign (opstaan uit zittende houding mbv hand)
        * difficulty w/ stair climbing
        * difficulty w/ toe walking
    + Less commonly:
        * Dx is considered as a result of developmental delay
        * Dx is considered as a result of hepatic dysfunction:
            - [enzymes]↑ => [alanine aminotransferase]↑ OR [aspartate aminotransferase]↑ OR [LDH]↑
    + Doctors' delay: Prompt referral to a neuromuscular specialist w/ input from geneticist/genetic counsellor, can avoid diagnostic delay.
3. Wat is de onderliggende Pax van DMD; kan je het aan iemand anders uitleggen?
    + ...
4. Waarom is genetische counselling belangrijk? Noem 2 redenen.
    + kinderwens
    + opsporing familieleden/carriers
5. Noem voors en tegens voor prenatale screening
    + ...
    + ...
6. voordelen van glucocorticosteroids?
    + ![glucocorticosteroids](./src/glucocorticosteroids.png)
7. Welke testen zijn predictief >7yo voor Fnx achteruitgang in ambulantie in het komende jaar; welke tests en afkapwaarden horen daarbij?
8. Hoe vaak worden controles door een revalidatiespecialist geadviseerd?
9. Hoe kan contractuurpreventie worden gedaan?
10. Noem voorbeelden hoe technische apparaten kunnen owrden ingezet om Fnx te optimaliseren.
11. 

![interventions](./src/interventions.png)

_ _ _ _

### HC FSHD ###

- Er zijn veel spierziektes; omdat je zoveel spier hebt, met veel verschillende functies:
    + mobiliteit/bewegen
    + digestie (SMCs)
    + ...

- FSHD
    + Sx:
        * endorotatio/protractie vh scapulo-humerale gewricht
        * grote ogen
        * slappe mond (laat pt. lippen tuiten)
        * wasting of humeral muscles (humerale atrofie)
        * winging and overriding scapula (weakness M. serratus)
        * asymmetric shoulder (65%)
        * facial weakness/assymetry (60%)
        * poly-hill sign
        * calf atrophy
        * drop foot (=> struikelen: door drop foot ; => vallen: later in ziekteprogressie kunnen pts. het struikelen niet opvangen (mbv. M. Quadriceps))
        * hamstring weakness
        * lumbar hyperlordosis
        * Beevor sign
            - M. rectus abdominus is niet homogeen aangedaan; bij aanspanning van deze spier gaat de navel omhoog (onderste gedeelte vd spier is vervet, bovenste gedeelte contraheert)
        * buik hangt (door zwakte vd M. rectus abdominus)
        * scoliosis (30%)
        * pectus excavatum
        * retinal vasculopathy
        * horizontal axillary fold
        * hearing loss
        * **asymptomatic 25%**
        * `CAVE:` false positive Dx: no criteria facial weakness and scapular weakness
    + Aex:
        * autosomal dominant myopathy (10% de novo)
            - D4Z4 repeats:
                + in populi: 8--100 units D4Z4
                + in FSHD1: 1--10 units (>95% vd mutatiedragers)
            - FSHD1 = chr.4[shortened D4Z4 + 4qA hypomethylation of D4Z4]
            - FSHD2 = chr.18[SMCHD1 mut] + chr.4[4qA hypomethylation of D4Z4]
        * L-R asymmetry of weakness (65%)
        * facial weakness (>80%)
        * onset: 2--50yo (mean = 17yo)
    + Epix:
        * prvl = 1/8000
            - capture-recapture method
        * incd = 0.3/100 000
        * prvl = 12/100 000
        * 2000 pts. in NL
        * `CAVE:` er is een relatie tussen symptomatiek en het genotype bij FSHD

- Fatigue in FSHD
    + pts. stoppen vaak met physical exercise ivm chronische ziekte
    + `less physical exercise => more fatigue`

- Tx (management)
    + cardiological examination when needed: no screening
    + respiratory monitoring when wheelchair dependent OR w/ pelvic girdle weakness AND pulmonary disease OR spinal deformity
    + scapula fixation: low risks, moderate gains
    + pain: standard pharmacoTx, physical Tx
    + fatigue: aerobic training
    + hearing examination: on indication except in infantile FSHD
    + retinal examination: yearly in infantile FSHD


### HC Duchenne ###

#### DMD ####

- Overview:
    + fout in dystrofine gen (X-linked recessive)
        * carriers hebben soms symptomen
            - `CAVE:` XO-vrouwen (S. Turner) kunnen aangedaan zijn (rare)
        * cardiopathologie komt wel veel voor (=> screening hiervoor)
    + dystrofine is belangrijk voor spierFnx
    + verlies spiercellen => bindweefsel/vet
    + progressieve spieraandoening
    + overleving verlaagd

- Tx:
    + antisense-induced exon skipping
        * matig succesvol
        * veel bijwerkingen
        * problemen met toediening Tx
    + m.b.v. prednison kunnen Sx 2--3y uitstellen
    + door prednison: scoliosis in 90% => scoliosis in 40% (= minder operatieve ingrepen)

- Sx:
    + eerste problemen: duuractiviteit/endurance/spiervermoeibaarheid
    + pseudohypertrophy: compensatoir mechanisme om spierweefsel zo functioneel mogelijk te houden d.m.v. vetstapeling
        * bij DMD o.a. in de kuiten
    + functionaliteit (i.e. fijne motoriek vingers) blijft heel lang goed

- Lopen gaat achteruit
    + compensatie accepteren
    + toename contracturen
    + voorzieningentraject (anticipatief)

- Dx:
    + leeftijd en 6min loopafstand: predictierf
    + 6min loopafstand < 350m => Px: rolstoelafhankelijkheid <1y
        * CAVE: aangepast huis kost 1 jaar om te regelen

- Assistive technology
    + loopfunctie is niet het belangrijkste voor pts; spierkracht in de armen is het meest invaliderend
    + robotarmen zijn gedeeltelijk effectief: pts krijgen geen sensory input, waardoor precies bewegen (heel) moeilijk word

- Clinx:
    + => HC pdf p.30

- 'Duchenne is topsport':
    + maximaal presteren in ADL
    + versnelde vermoeibaarheid
    + aanpassen bij progressief beeld
    + medische controles
    + hard werken aan lijfbehoud
    + beperkte mogelijkheden voor sport
    + mentale fitheid belangrijk!

- Px:
    + avg lifetime: 35yo (gestegen sinds 70s: 20yo => 35yo )
    + endocrino problemen gedurende maturatie en corticosteroids
        * osteoporosis
        * groei retardatie
        * testosteron maturatie retardatie
        * calcium, vit.D, bisfosfonaten, testosteron
    + Tx met:
    + bewegen met een spieraandoening:
        * duursporten zijn (geheel) onschadelijk
        * intensieve krachttraining / eccentrische bewegingen zijn wel funest voor DMD-pts.
    + (elektrische) rolstoelafhankelijkheid:
        * osteoporosis
        * scoliosis
        * obesitas
        * afname arm/handFn
        * voedingsproblemen
        * obstipatie
        * mictieproblemen
        * afname longfunctie
        * cardiomyopathy
    + CAVE: zithouding is zeer belangrijk voor kwaliteit van leven
        * zitten in een rolstoel met scoliose is terrible => decubitus, fatigue, etc.
    + cognitieve aspecten:
        * verbal IQ`<`performaal IQ
        * allemaal verminderd short-term memory voor verbaal begrip en visuospatial tests
        * difficulty w/ face recognition, meer soc. gedragsproblemen dan brusjes of kinderen met CP
        * vaker autistiforme stoornissen
        * hogere incd dyslexia
        * `CAVE:` naar school gaan is moeilijk, aanpassingen zijn nodig (anders krijgen de DMD-pts. het niet voor elkaar). Het is heel zeldzaam dat DMD-pts. naar een reguliere middelbare school; de meesten moeten (uiteindelijk) naar een mytyl-school.


_ _ _ _

### WG4 DMD ###

#### Casus 1 ####

- Welke problemen kan pt. tegenkomen:
    + medisch
    + psychisch
    + sociaal
- med. problemen
- leervermogen
- taakuitvoering
- communicatie
- mobiliteit (onderste+bovenste extremiteiten)
    + CAVE: fysioTx is m.n. gericht op het verminderen/uitstellen van contracturen
- zelfverzorging (kleden+toiletgang+eten/drinken)
- huishouden
- pers. relaties
- maatschappelijk (school+hobby+sport)
- meerdere
- rolstoelafhankelijk
- obesitas?
- ...
- IQ-related 

- Dx: DMD
    + DNA onderzoek naar dystrofine-gen
        * als niets wordt gevonden
- Fnx:
    + opstaan
    + langdurig zitten
    + schrijven
    + lopen
    + lezen
    + vermoeidheid
    + evenwicht?
    + verminderde duurconditie
    + traplopen
    + kuitspierhypertrofie
    + contracturen
- Activiteiten
    + loopt moeilijk
    + minder lang schrijven
- soc. ptc.
    + sporten
    + spelen
    + school/educatie
- long-term Px Fnx:
    + onderste extremiteiten nu al aangedaan
    + sterke Fnx-beperking als armen 
- long-term Px mobility:
    + progressief slechter
    + CAVE scoliosis
    + ADL-afhankelijk
- long-term Px ADL:
    + afh. van mobility prognose (maar blijft wel zelfstandig kunnen voortbewegen)
    + onderste extremiteiten nu al aangedaan => CAVE: start achteruitgang armkracht.
    + mbv hulpmiddelen
- long-term Px soc. ptc.
    + minder sporten/spelen, m.n. duursporten aangedaan
    + vrienden/mobiliteit
    + rolstoel in auto / OV
    + school/educatie
- revalidatieplan
    + multidisciplinair team samenstellen
    + logopedie => gezichtsmusculatuur vervet progressief; maar vaardigheden (slikken, kauwen, praten) blijven erg lang intact.
    + fysioTx
    + psychoTx
    + diëtist
    + predniso(lo)n 0.75mg/kg/d
    + mobility assist (rolstoel, spalken, etc.)
    + evt. chirurgisch ingrijpen indien contractuur
    + aangepast huis?

#### Casus 1 cont. ####

- hulpvraag?
    + is er iets te doen aan mictieklachten?
    + meer/betere aanpassing m.b.v. hulpmiddelen
- Fnx:
    + toiletgang
    + aankleden
    + lopen
    + traplopen
    + hamstrings verkort
    + spitsvoet
    + knie extensie
    + mictie
    + kracht armen
- Activiteiten
    + uittrekken kleding
    + fietsen
    + traplopen
    + zwemmen?
    + opstaan
- soc. ptc.
    + spelen met vriendjes
    + drummen
    + buitenspelen
    + `CAVE:` mictiestoornis leidt obv. tot veel soc.ptc.-problemen
- Px mobiliteit 1--2y
    + volledig rolstoelafhankelijk => elektrische rolstoel
- Px soc.ptc middel-lange termijn:
    + verminderde actieradius
- Tx:
    + 
- adviezen t.a.v. draagsterschap?
    + verwijzing cardioloog incl. moeder
    + evt. psycho-educatie genetisch (kinderwens)

#### Casus 1 cont. ####

- hulpvraag
    + obesitas? hulp
    + prednison bijwerkingen?
    + contracturen? chirurgie?
- activiteiten beperking:
    + schrijven helemaal aangedaan
    + zelfverzorging
    + rolstoelafhankelijkheid
    + sporten
    + lezen?
- Fnx:
    + toiletgang
    + lopen
    + schrijven
    + ADL behalve tandenpoetsen
    + armfunctie
    + scoliose
    + zithouding / evenwicht
- soc. ptc.:
    + aangepast sport
- advies / beinvloedbare Fnx-stoornis/beperkingen
    + armondersteuning
    + gewicht
    + aanpassingen rondom school
    + aangepast toilet
    + sporten/gym?
    + psycho-educatie omstanders/mantelzorgers

_ _ _ _

### HC Revalidatie anamnese (Ax-reva) ###

- Spreekkamer / consult
    + Ax conform ICF
    + onderzoek stoornis en activiteiten
    + fuctionele testen
    + opstellen behandelplan
    + medische handelingen

- Team
    + observatiefase
    + behandelplan is volgens SMART
    + achtergrondinfo geven
    + samen kijken naar kind
    + afspreken wie welk contact heeft
    + evaluaties

- technische spreekuren
    + schoenen-spreekuur
    + orthesen-''
    + prothese-''
    + bewegingsanalyse/looplab

- multidiscip spreekuren
    + spina bifida
    + CP
    + neurodegen aandoeningen
    + bewegingsstoornis
    + NMA
    + hersentumoren
    + neuro-immuun aandoeningen
    + groeistoornissen

- revalidatieteam:
    + => zie pdf HC anamnese
    + Fysiotherapeut 
    + Ergotherapeut 
    + Logopedist 
    + Maatschappelijk werk 
    + Psycholoog/orthopedagoog 
    + Leerkracht mytylschool 
    + Ambulante begeleider 
    + Speltherapeut 
    + Bewegingsagoog 
    + Hydrotherapeut 
    + Revalidatiearts 

![RAPP-triangle](./src/RAPP-triangle.png)

- ad. 'Hulpvragen':
    + mondigere, goed geinformeerde ouders / jongeren
    + aanbodgerichte zorg soms nodig
    + nieuwe manieren van hulpvraaginventarisatie
        * COPM
        * motivational interviewing

- **COPM (Canadian Occupational Performance Measure)** is de main bread and butter in de revaGNK
    + De COPM heeft twee doelen:
        * Identificeren van de belangrijkste problemen die de cliënt ervaart
        * Meten van veranderingen in het beeld dat de cliënt heeft van zijn handelen gedurende het behandelprocesproces.  
    + De COPM richt zich op drie gebieden:
        * Zelfredzaamheid
        * Productiviteit
        * Ontspanning.
    + De COPM kan ook gebruikt worden om de hulpvraag van de cliënt te verhelderen
    + semi-structured interview

- MI (motivational interviewing)
	- motiverende gespreksvoering
	- op samenwerking gerichte gesprekstijl
	- versterken motivatie en bereidheid tot verandering
	- verkennen en verminderen van ambivalentie

- Clientgerichte benadering
	- client geeft richting aan gesprek
	- client is expert
	- verantwoordelijkheid bij client
	- hulpverlener is helper bij maken van beslissingen en keuzes
	- in samenspraak tot beleid komen/onderhandelen
	- LUISTEREN is belangrijker dan informatie geven

#### Revalidatiemodellen ####

- SAMPC-model
    + **S**oma
    + **A**DL
    + **M**aatsch.
    + **P**sych Fnx
    + **C**ommunicaties
- SAMPC vs ICF:

|         probleem        |   ICF    |   SAMPC/SFMPC   |
|-------------------------|----------|-----------------|
| hemiparese              | stoornis | soma            |
| niet zelfst. toilet     | activity | ADL             |
| afhank. is vervelend    | activity | ADL/Functioneel |
| geen zelfst. toiletgang | soc.ptc  | Maatschappelijk |

- RAP (revalidatie activiteiten profiel)
    + hulpvraaggericht
    + gezamenlijke doelen formuleren en evaluerenm
    + teambespreking: dialoog met cliënt
    + multidisciplinair
    + gemeenschappelijke taal: RAP-domeinen
        * afgeleid van ICIDH
            - stoornJggis / functies
            - beperking / vaardigheden
            - handicap / participatie
    + accent op activiteiten (niet stoornissen)

- Groeiwijzer
	+ model t.b.v. bewoordeling zelfstandigheidsontwikkeling
	+ vaardigheden voor zelfstandig leven worden stapasgewijs ontwikkeld. Is natuurlijk proces, dat ontstaat door voortdurend afstemmen tussen kinderen/jongeren, ouders en omgeving.
	+ door een beperking stagneert dit proces soms en verloopt het anders
	+ Domeinen:
	+ Ik : zelfregie, eigen verantwoordelijkheid
	+ Studie : huiswerk leren, vervolgopleiding
	+ Sport : gezond bewegen
	+ Vervoer : zelfstandig ergens heen kunnen
	+ Vrije tijd : hobby, clubs, activiteiten, vakantie
	+ Werk : bijbaantje, werk, dagbesteding
	+ Wonen : ADL, huishouden, financien, uit huis?
	+ Relaties : vriendschap, relaties, sexualiteit
	+ Zorg : zelfregie, kennis aandoening
- beperkingen vh model:
	+ IQ > 70 is vereiste
	+ vragen zijn niet zonder meer geschikt voor de groep met een verstandleijke beperking
	+ grote diversiteit in deze groep
	+ is geen meetlat

_ _ _ _

### HC Groei ###

#### Casus 1 ####

- spina bifida -- myelomeningocele
    + verlamming onderlichaam
    + urine-incontinentie
    + obstipatie
    + scoliosis
    + rolstoelafhankelijk
    + hydrocephalus?
    + ontw.achterstand?
    + epilepsie?

- welke problemen kun je verwachten bij het meten van de lengte bij Bram?
    + contracturen
    + niet kunnen staan
    + scoliosis


- Normaliter groeien de botten als reactie op mechanische kracht ; bij verlamming (bij kinderen) is er geen mechanische kracht op de verlamde lichaamsdelen => zorgt voor achterstand in groei
- CAVE: drukverhoging in cerebro kan zorgen voor hypofyse/(hypo)thalamus schade.

_ _ _ _

### WG5 Groei ###

#### CASUS 1 ####

Tim, een 13 jarige jongen met spina bifida, waarbij hij verlamd is aan zijn onderlichaam en een beginnende scoliose heeft, komt met zijn ouders op het spreekuur. Zijn ouders vertellen dat hij vaak geholpen moet worden, vooral bij verplaatsing van rolstoel naar bed/toilet. Dit wordt steeds lastiger nu Tim in de puberteit is, omdat hij steeds langer en zwaarder wordt. De revalidatie arts verwijst naar de kinderendocrinoloog met verzoek tot groeiremming. Ouders komen op het spreekuur met de vraag of u de groei van Tim kunt en wilt stoppen. Zijn huidige lengte is 175 cm.

- Welke problemen kunnen optreden bij patiënten met spina bifida ten gevolge van spierzwakte of verlamming?
    + niet meer lopen
    + minder mobiel
        * transfers zijn moeilijk
    + afhankelijkheid
    + GI-symptoms
    + overweight
    + cogn./behav. problemen bij hydrocephalus

- Op welke manier zijn deze problemen door de revalidatie arts aan te pakken?
    + (electrische) rolstoel
    + fysioTx
    + monitoring progressie scoliose
    + 

- Waarom verwijst de revalidatie arts voor groeiremming? Waarom willen ouders groeiremming?
    + arts: toename (ernstige) scoliose is indicatie voor groeiremming ; grotere lengte gaat hem niets brengen ; Tim is op deze leeftijd nog vóór de groeispurt in de puberteit.
    + ouders: Tim is niet meer te tillen
    + `CAVE` groeiremming kan ook de puberteitsontwikkeling remmen

- Is het mogelijk om de groei te remmen/stoppen? En zo ja, op welke manier?
    + ja, mogelijk
    + hormoonreceptor-antagonisten (testosteron => GnRH-suppressie ?)
    + percutane epifysiodese
    + als je de puberteit mist, wordt je piek-botmassa (uiteindelijk) ook minder hoog. Bij spina bifida heb je al een verminderde botdichtheid. => complicaties

- Welke informatie mist in de verwijzing om een goed advies te kunnen geven?
    + puberteitsstadium (Tanner stage)
    + wat de wens van Tim is
    + FamAx
    + X-ray hand (beoordelen pathologische/fysiologische groei, beoordeling botleeftijd)

- Stel jij voorspelt dat Tim nog 20 cm zal groeien, Zou jij adviseren om de groei van Tim te remmen? Wat zijn je overwegingen?
    + ja
    + toename scoliosis
    + genetisch bepaalde lengte
    + pathologische hypergroei?
    + hulpvraag


Bronnen

- Bron werkboek kinderendocrinologie: grote lengte
- Advanced skeletal maturity in children and adolescents with myelomeningocele -- Roiz et al (2017)
- Accuracy of final height prediction and effect of growth-reductive therapy in 362 constitutionally tall children –- de Waal et al (1996)

#### CASUS 2 ####

Een 10-jarige jongen met Duchenne spierdystrofie komt op het spreekuur. Hij heeft last van spierzwakte van heupen, waardoor hij een opvallend looppatroon heeft. Binnenshuis loopt hij nog korte stukjes, verder is hij rolstoelafhankelijk. De kracht in de armen is nog goed. Er is sprake van hypotonie en heeft hij een milde scoliose. In verband met gedragsproblemen gebruikt hij geen corticosteroïden. Een dag later komt een 10 jarige jongen met Prader Willi syndroom op het spreekuur. Ook hij heeft hypotonie en een milde scoliose. De hypotonie en scoliose zijn bij beide jongens op dit moment even ernstig.

- Wat verwacht je van de progressie van de scoliose over de komende jaren bij bovenstaande 2 patiënten? Wat zijn overeenkomsten en verschillen tussen beide patiëntengroepen?
    + DMD: scoliose-progressie van DMD is ernstiger, vanwege rolstoelafhankewlijkheid en spierdystonie/dystrofie; bij PWS is dat niet. Bij DMD is er progressieve spierzwakte ; 
    + bij PWS worden kinderen sterker naarmate de leeftijd vordert => minder progressie scoliose ~~(PWS: geen progressie scoliose)~~
    + verschillen: DMD is X-linked recessive, PWS: partial chrom.15 loss/gain in utero ; meestal de novo mutatie.

- Welke niet-invasieve behandelopties zijn er? Is deze behandeling effectief?
    + fysioTx
    + brace/spalk (in praktijk weinig effectief)
    + psychoTx t.b.v. behav. problems
    + goede zithouding

- In welke situaties zou je kiezen voor operatieve correctie?
    + scoliose >40̉deg of bij ernstige symptomatiek OR
    + ernstige symptomatiek
    + `CAVE:` ademhalingsproblematiek (compressie longen door scoliosis)
    + ==> er mag alleen maar geopereerd wordsen bij voldoende respiratoire reservecapaciteit ; obesitas veroorzaakt vaak wondcomplicaties post-operatief

- Op welke manier kun je de groei monitoren bij patiënten met scoliose?
    + X-ray
    + spanwijdte meten
    + ulna-lengte (evt. tibia-lengte)
    + zithoogte

- Hoe kun je progressie van scoliose monitoren?
    + herhaaldelijk X-ray (jaarlijkse controle)

Bronnen

- Corticosteroids for the treatment of Duchenne muscular dystrophy – Matthews et al
- Landelijke richtlijn diagnostiek en behandeling van kinderen met het Prader Willi syndroom (2013)
- Scoliosis in patients with Prader Willi syndrome – comparisons of conservative and surgical treatment – Weiss, Goodall (2009)

#### CASUS 3 ####

Een 14-jarige jongen met Duchenne spierdystrofie, behandeld met Deflazacort, komt op het spreekuur. Hij heeft een kleine lengte en is nog niet in de puberteit. Ouders vragen zich af of het mogelijk is de puberteit verder uit te stellen, omdat hij nu zo makkelijk te hanteren is (zowel fysiek als mentaal).

- Wat zou de oorzaak van kleine lengte kunnen zijn?
    + corticosteroid usage
    + dystrofine-gen predisponeert voor kleine lengte
    + genetisch / erfelijk
    + psychosociaal

Behalve het vervolgen van de lengte is ook vervolgen van het gewicht bij patiënten met Duchenne belangrijk. Zij kunnen zowel obesitas als ondergewicht ontwikkelen. Hoe kun je dit verklaren en welk stadium van de ziekte verwacht je dit?

- Wat zou je kunnen doen aan obesitas en ondergewicht bij deze patiënt?
    + weight loss: afhankelijkheid, spieratrofie sec., door resp. insuff (kost veel energie) kunnen DMD-pts ineens zeer veel afvallen in korte tijd.
        * dieet, dietist
    + weight gain: cortico + minder beweging, spieratrofie => lager BMR => meer weight gain bij dezelfde intake.
        * medicijnsanering, sporten, dietist

- Wat kan het uitblijven van puberteit verklaren?
    + cortico's 

- Kun je de puberteit remmen?
    + ja

- Zou je dit doen? En waarom wel/niet?
    + fuck no, ouders willen kunnen tillen = is geen goede reden

- Op welke manier draagt revalidatie bij aan verbetering van het ziektebeloop?
    + beweging
    + fysioTx t.b.v. handfunctie
    + psycho-soc.

Bronnen

- Short stature and pubertal delay in Duchenne muscular dystrophy – Wood et al (2016)
- Duchenne Richtlijn 2018 
    + Diagnosis and management of Duchenne muscular dystrophy, part 1: diagnosis, neuromuscular, rehabilitation, endocrine, and gastrointestinal and nutritional management – Birnkrant et al (2018)
    + Diagnosis and management of Duchenne muscular dystrophy, part 2: respiratory, cardiac, bone health, and orthopaedic management - Birnkrant et al (2018)
    + Diagnosis and management of Duchenne muscular dystrophy, part 3: primary care, emergency management, psychosocial care, and transition of care across the lifespan Birnkrant et al (2018)


_ _ _ _

_ _ _ _

### PR Neurologisch Onderzoek 1 ###

#### reflexen ####

- bicepspeesrelfex
- brachioradialis peesreflex
- triceps peesreflex
- knie peesreflex
- achillespeesreflex
- Babinski reflex

- 'uitgebreide' reflex = opwekking van een reflex bij stimulatie (aanslaan met reflexhamer) van een gebied, waar deze reflex helemaal niet opgewekt hoort te worden = het gebied waar de reflex opwekbaar is is vergroot = pathologische reflex
- hyper-/hypo-/a-/dys-reflexia/clonus

#### tonus ####

- Tonus wordt beoordeeld door passieve beweging

#### coordinatie ####

- topneus
- top-topproef
- disdiadochokinese
- hielknieproef
- koordansersgang
- Romberg

#### functioneel testen ####

- tenen lopen
- hakken lopen
- squats
- opstaan uit schuttersstand
- opstaan uit lig/zit
- looppatroon
- Brooke armfunctie
- schrijffunctie (test ook de fijne motoriek)

#### neurologische aandoenignen; sensor + motor ####

- Beoordeling m.b.v. ASIA ()
- dwarslaesie
- spina bifida
    + `CAVE:` bij spina bifida zijn de afwijkingen anders dan bij een dwarslaesie: want spina bifida is een _aanlegstoornis_

#### sensibiliteit ####

![Medulla_spinalis_tracti](https://upload.wikimedia.org/wikipedia/commons/f/fe/Medulla_spinalis_-_tracts_-_English.svg)

- tastzin (in ASIA)
- pijnprikkel (in ASIA)
- vibratiezin
    + m.b.v. stemvork op malleolus => daarna vergelijken met de eigen vibratiezin
- bewegingszin/propriocepsis
    + tenen bewegen, zonder dat pt. het ziet => vragen naar positie vd tenen en/of vragen of ze bewegen of niet
- (Romberg)

_ _ _ _

### ZSO neurologisch onderzoek ###

#### NO: motor -- spierkracht en selectiviteit ####

- Spierkracht meet men m.b.v. de _MRC-schaal_ (zie eerder)
- **Selectiviteit** is het vermogen om een gewricht, onafhankelijk van houding, apart te kunnen bewegen, t.o.v. andere gewrichten in hetzelfde lidmaat. (wordt gescoord met: `wel/niet (selectief)`)
    + De kracht mag alleen worden beoordeeld als de gevraagde beweging _selectief_ kan worden uiitgevoerd.
    + **Synergie beweging**: als pt. alleen in een flexie of extensie patroon kan bewegen

- Interpretatie
    + krachtsvermindering? zo ja, welk type? (spieraandoening, aandoening perifere motorische neuron, aandoenign centrale motorische neuron)
    + krachtsvermindering + hyperreflexie en/of spasticiteit in aangedane zijde? => **centrale laesie** waarschijnlijk.
    + krachtsvermindering + hypotonie _+ sensibiliteitsverlies_ + hypo-/a-reflexie? => **perifere zenuwlaesie** waarschijnlijk
    + krachtsverlies + tonusverlies + hypo-/a-reflexie -- _GEEN sensibiliteitsverlies_ +/- fasciculaties (vaak aanwezig)? => **aandoening motorische voorhoorncellen**
    + krachtsverlies + tonusverlies + hypo-/a-reflexie _+ aandoening is (meer) symmetrisch en in meer proximale spiergroepen gelokaliseerd_? => **spieraandoening** of **aandoening vd motorische eindplaat**.

#### NO: motor -- spiertonus in rust ####

- Spiertonus beoordelen door passieve beweging (e.g. de arm)
    + pt. ligt zo ontspannen mogelijk op de onderzoeksbank
    + pak onderarm bij de pols, elleboog rust op onderzoeksbank
    + leg vingers op de bicepsbees
    + buig/strek enkele malen de elleboog
    + vergelijk links en recht

- Interpretatie
    + **rigiditeit**: weerstand over het gehele bewegingstraject (lodenpijp-fenomeeen) en/of verloopt met kleine schokjes (tandradfenomeen). => wijst op een aandoening vh extrapiramidale systeem.
    + **spasticiteit**: er bestaat alleen weerstand over een niet-constant gedeelte vh bewegingstraject, bij krachtig doorbewegen verdwijnt de weerstand => wijst op een aandoening vh centraal motorisch neuron (piramidaal systeem)
    + **hypotonia**: verminderde rust-tonus vd spier => komt voor bij onderbreking vd prikkelgeleiding naar de spier (e.g. ernstig perifeer zenuwletsel / piramidebaanletsel in het acute stadium). Bewust aanspanning vd spier is ofc. ook onmogelijk in dit geval.

#### NO: motor -- onderste extremiteit ####

- Mm. glutei medii en minimi (L4, L5, S1, N. gluteus superior)
    + Bij mobiele pts:
        * pt. staat op één been
        * Beoordeel of pt. het bovenlichaam over het standbeen laat hellen (= pos. sign of Duchenne)
        * beoordeel of pt. het bekken horizontaal kan houden (niet mogelijk = pos. sign of Trendelenburg)
    + Beide tekens/signs kunnen wijzen op een krachtverlies van bovengenoemde spieren vh standbeen.
    + Bij bedlegerige pts:
        * breng het been (aan de te onderzoeken kant) passief in abductio
        * vraag de pt. het been in deze positie te houden en probeer het been te adduceren.
    + Indien de pt. het been neit in abductio kan houden, wijst dit op krachtsverlies vd onderzochte spieren.
- M. ilipsoas (Th.12, L1, L2, L3, N. femoralis)
    + pt. ligt op de onderzoeksbank en houdt de heup in maximale flexie (knie tegen de borst)
    + beoordeel kracht door te proberen de heup weer in neutrale stand te brengen
- M. quadriceps (L2, L3, L4, N. femoralis)
    + leg het gestrekte been vd pt. op je onderarm, terwijl je hand steunt op de knie vh contralaterale been
    + beoordeel kracht door te proberen het been te buigen in de knie.
- Mm. adductores femoris (L2, L3, L4, N. obturatorius)
    + pt. ligt, buigt heupen en knieën en houdt de knieën stevig tegen elkaar
    + beoordeel kracht door te proberen de knieën van elkaar te trekken (abduceren)

#### NO: motor -- bovenste extremiteit ####

- vermindering/uitval vd prikkelgeleiding naar een spier (of spieraandoening zelf) => krachtverlies en atrofie
- stoornis in extrapiramidale systeem => hypertonia + onwillekeurige bewegingen
- onderzoek vd motoriek vd pt moet de volgende vragen beantwoorden:
    + is er krachtverlies? zo ja, in welke mate?
    + is de oorzaak het gevolg van:
        * aandoening vd spier zelf of de motorische eindplaat?
        * aandoening vd perifere zenuw?
        * aandoening centraal motorisch neuron?
    + is er verandering vd spiertonus?
    + zijn er onwillekeurige bewegingen?

##### NO: bovenste extremiteit -- motor #####

- M. serratus anterior (C5, C6, C7, N. thoracalis longus)
    + pt. staat met armen tegen de meur geleund, met de handen op navelhoogte
    + beoordeel of de scapulae tegen de thoraxwand aan blijven liggen
- M. deltoideus (C4, C5, C6, N. axillaris)
    + pt. heeft de armen opzij gestrekt en probeert ze zo te houden
    + beoordeel kracht door beide armen naar beneden te duwen
- M. biceps brachii (C5, C6, N. musculocutaneus)
    + pt. houdt arm maximaal gebogen in de elleboog, onderarm in supinatie
    + beoordeel kracht door de arm proberen te strekken
- M. triceps brachii (C7, C8, N. radialis)
    + pt. houdt armen maximaal gestrekt in de elleboog
    + beoordeel kracht door de arm proberen te buigen
- Extensoren vd pols (C6, C7, N. radialis)
    + pt. houdt de pols in extensie en de onderarm in pronatie
    + beoordeel kracht door de pols proberen te flecteren.
- Flexoren vd pols (C7, C8, Th1, N. medianus)
    + pt. legt onderarm op tafel in supinatie, maakt een vuist en flecteert de pols.
    + beoordeel kracht door de pols proberen te strekken.
- Flexoren vd vingers (C7, C8, Th1, N. medianus)
    + Laat pt. zo hard mogelijk in uw uitgestoken wijs- en middelvinger knijpen
- Extensoren vd vingers (C7, C8, N. radialis)
    + pt. houdt de vingers gestrekt
    + beoordeel kracht door proberen de vingers te buigen in het metacarpophalangeale gewricht.
- M. opponens pollicis (C6, C7, N. medianus)
    + pt. houdt duimtop en pinktop zo stevig mogelijk tegen elkaar
    + beoordeel kracht door proberen de ontstane cirkel (duim+pink) te doorbreken.
- Mm. interossei (C8, Th1, N. ulnaris)
    + pt. strekt en spreidt de vingers
    + doe hetzelfde en breng vingers tussen die vd pt.
    + vraag pt. zo krachtig mogelijk de vingers te slutien
    + _links en rechts tegelijkertijd_! ter vergelijk.

#### Roessingh klinische testen ####

- Heupflexoren
    + M. iliopsaos
        * M. iliacus (mono-articulair)
        * M. psoas   (bi-articulair)  
- Heupextensoren
    + M. gluteus maximus
    + hamstrings
        * M. semimembranosus
        * M. semitendinosis
        * M. biceps femoris

_ _ _ _

### ZSO-OPO ###

Zelfstudie opdracht orthopedisch onderzoek

#### Inleiding ####

In deze zelfstudie opdracht haal je je kennis op van het lichamelijk onderzoek van het bewegingsapparaat. Vervolgens ga je je verder verdiepen in het onderzoek van de enkel/voet en gaan we dieper in oorzaken van schouderpijn na hersenletsel. In het practicum zul je dit onderzoek op elkaar gaan oefenen. 

#### Leerdoelen ####

- Je kent het onderzoek van het bewegingsapparaat van de heup, knie en schouder.
- Je maakt kennis met het onderzoek van de enkel/voet en de belangrijkste aandachtspunten daarbij.
- Je kent de belangrijkste klachten van de schouder als gevolg van hersenletsel kent de behandeling hiervan.

#### Onderzoek van de heup, knie en schouder ####

- Fris je kennis op van het onderzoek van het bewegingsapparaat van de heup, knie en schouder. Ga hiervoor terug naar de lesstof en je eigen aantekeningen van de leerlijn psychomotore vaardigheden van Q7 tot en met 10. Beantwoord de volgende vragen:

- In welke richtingen zijn er bewegingen mogelijk in heup, knie en schouder? Noteer de bewegingsuitslagen volgens de methode deBrunner (zie practicum inleiding orthopedisch onderzoek en blz. 5 van syllabus voor lichamelijk onderzoek bij gangbeeld analyse uit Enschede)
- Welke spieren zijn de belangrijkste bewegers (prime movers) van iedere richting?
- Wat is het capsulaire patroon van ieder gewricht? Wat betekent het als je gewrichtsbeperkingen binnen een capsulair patroon vind?


#### Onderzoek van de enkel/voet ####

- Bekijk het bewegingsonderzoek onderzoek van de enkel/voet in de syllabus voor lichamelijk onderzoek bij gangbeeld analyse uit Enschede.
- Beantwoord de volgende vragen.

- Welke 4 gewrichten kennen we in de voet
- Benoem de bewegingsuitslagen van ieder gewricht.
- Welke spieren zijn de belangrijkste bewegers van deze bewegingen?
- Wat is de normale range of motion van ieder gewricht? (Bekijk goed hoe je de gewrichtsmobiliteit meet)
- Wat is het doel van het meten van de dijbeenvoethoek en de voetlijn?
- Op de stand van welk bot let je bij de beoordeling van de statiek van de achtervoet.
- Waarom is het van belang om te beoordelen waarom de stand van de voet corrigeerbaar is of niet?

#### Specifieke problemen van de schouder bij hersenletsel ####

- Lees het artikel van Manara et al.
- Beantwoord de volgende vragen:

- Welke oorzaken van schouderpijn na hersenletsel zijn er?
- Welk lichamelijk onderzoek en aanvullend onderzoek doe je om de oorzaak van schouderpijn na hersenletsel te achterhalen? Wat wil je daar mee aantonen?
- Welke behandelmogelijkheden bestaan er voor iedere oorzaak van schouderpijn?

- [Instructievideo_intra-articulaire_injectie_schouder](https://www.youtube.com/watch?v=op8pL8c9UBM)


Aanvullende informatie bij zelfstudie opdracht orthopedisch onderzoek 
 
#### Gewrichtsmobiliteit / uitslagen ####
 
Onderstaande waarden zijn de standaardwaarden volgens de American Association of Orthopedic Surgeons.

**De spieren die onderstaande beweging uitvoeren staan hierboven (NO -- **\* extremiteit: motor)**
 
| Schouder | max/neu/max |
|----------|-------------|
| flex/ext | 180/0/60    |
| abd/add  | 180/0/75    |
| endo/exo | 90/0/70     |
 
|   Heup   | max/neu/max |
|----------|-------------|
| flex/ext | 120/0/30    |
| abd/add  | 45/0/30     |
| endo/exo | 45/0/45     |
 
|   Knie   | max/neu/max |
|----------|-------------|
| flex/ext | 150/0/0     |
 
|  Enkel[^4]   | max/neu/max |
|--------------|-------------|
| flex/ext[^1] | 20/0/50     |
| var/valg[^2] | 10/0/10[^3] |

**See also:** [Wiki -- motion jargon](https://en.wikipedia.org/wiki/Anatomical_terms_of_motion)


##### Capsular patterns #####



![capspattweb](https://www.physio-pedia.com/images/6/65/Joint_Capsular_Patter.jpg)


_ _ _ _

### LO -- checklist in volgorde ###

#### part 1: zit ####

- glenohum mobility
-
- apr
- kpr
-  

### PR-FnO -- miniBESTest ###

- task 1: sit to stand
    + pt. sits on chair, arms crossed
    + ask pt to stand up, note whether pt.:
        * uses hands, 
        * multiple attempts to rise, 
        * needs assistance
        * touches the chair with the back of their legs
    + SCORING:
        * (2) normal: comes to stand without use of hands and stabilizes independently.
        * (1) moderate: comes to stand WITH use of hands on first attempt.
        * (0) severe: unable to stand up from chair without assistance OR needs several attempts WITH use of hands.
- task 2: rise to toes
    + pt. looks at non-moving target
        * allow pt. to try rising to toes TWICE
        * if you suspect pt. is not rising to full height; offer to let pt. try while holding your hands
    + SCORING:
        * (2) normal: stable for 3s with max height.
        * (1) moderate: heels up, but not full range (smaller than when holding hands) OR noticeable instability for 3s.
        * (0) severe: <3s
- task 3: stand on one leg
    + have pt. stand on one leg w/ hands on their hips
    + record number of seconds pt. is able to stand on one leg for a max of 30s. 
        * stop timing when pt. moves their hands off their hips or puts a foot down.
        * allow two attempts, record the best.
    + SCORING:
        * (2) normal: 20s
        * (1) moderate: <20s
        * (0) severe: unable
- task 4: forward
    + stand in front and to the side of the pt, with one hand on each shoulder
    + ask pt to push forward, require them to lean until their shoulders and hips are in front of their toes
    + suddenly release your push when pt. is in place
    + this test MUST elicit a step
    + SCORING:
        * (2) normal: recovers independently with a single, large step (second realinment step is allowed)
        * (1) moderate: more than one step used to recover equilibrium.
        * (0) severe: no step OR would fall if not caught OR falls spontaneously
- task 5: backward
    + stand behind and to the side of the pt. with one hand on each scapula, require them to lean until their shoulders and hips are behind their heels.
    + this test MUST elicit a step
    + SCORING:
        * (0) normal: recovers independently with a single, large step.
        * (1) moderate: 
        * (2) severe: no step OR would fall if not caught OR falls spontaneously
- task 6: lateral
    + stand to the side of the pt. on left or right side and to the side of the pt. with one hand on each scapula, require them to lean until their shoulders and hips are behind their heels.
    + this test MUST elicit a step
    + SCORING:
        * (0) normal: recovers independently with a single, large step.
        * (1) moderate: 
        * (2) severe: no step OR would fall if not caught OR falls spontaneously

_ _ _ _

_ _ _ _

### HC Het ontwikkelende brein (2020-02-17 0830) ###

#### Embryogenesis ####

- cerebrum, huid, nagels, haren en tanden ontstaan allen uit het _ectoderm_
- neurofibromatosis 1
    + prvl: 1:3000 (mensen)
    + autosom. dom.
    + \>5 'cafe-au-lait' vlekken (derma) AND een ander kenmerk:
        * freckling
        * optic pathway glioma
        * tibial bowing
        * 1e graads fam. lid met NF1
- (Tubereuze sclerose -> "minder waarschijnlijk op tentamen" -- Schieving)
    + zie pdf uit HC
    + ad. (Coenn's fibroma is een symptom)

- Timeline zwangerschap
    + neurale buis sluit al @ 4w zwanger
        * incomplete sluiting craniaal: _encephalocele_
        * incomplete sluiting caudaal: _spina bifida_
    + na vorming neurale buis -> neurale proliferatie

- Infections (risks when pregnent or pre-pregnant)  
    + mnemonic: **ToRCHeS-Z**
        * toxoplamosis
        * rubella
        * CMV
        * Herpes
        * Syphilis
        * Zika-virus
    + adequate sshielding is recommended for pregn. women (gloves, mouth mask, disinfection)

- Cerebrogenesis[^6]  
    + ziekte tgv foutieve neuro-migratie in cerebro
        * lisencephalopathy (male) -> wiki -> death ≤1--2yo
        * double cortex (female) -> wiki -> severe disability
        * tuberous sclerosis -> wiki -> disability, but limited
    + ziekte tgv foutieve groei/development vd gemigreerde cellen (= "_Overgrowths_")
        * megaencephale: te veel hersencellen -> te groot voor de schedel -> cerebrum buiten schedel


![braindev-structures](./src/braindev-structures.png)

ad. Cerebrum tree -> HC .pdf pros/dis/rhomb/- encephalon

![braindev-MRI-normal](./src/braindev-MRI-normal.png)

![braindev-MRI-rare](./src/braindev-MRI-rare.png)


![braindev-neurogenesis](./src/braindev-neurogenesis.png)


![braindev-structures](./src/braindev-structures.png)


- Myelinisation
    + peripheral: door Schwann cells
    + CNS: door oligodendrocytes

![braindev-myelinisation](./src/braindev-myelinisation.png)

- Stappen in ontwikkeling (by increasing complexity)
    + (1) zintuiglijke ontwikkeling
    + (2) motor + sensor ontwikkeling
    + (3) taal + cognitieve ontwikkeling
    + (4) sociaal-emotionele ontwikkeling 

- Motor ontwikkeling    
    + dia's 'motore ontwikkeling' -> timeline dev/T
    + lopen in NL: 12-18mn (N)[^7]
    + het voorkomen van wiegendood -> "baby niet op de rug liggen"
        * hiervoor zijn allemaal zitjes om echte rugligging te voorkomen
        * GEEN goede practice: kinderen die zó geholpen worden, krijgen een motor achterstand (lopen pas ≥2yo)

![child-motor1](./src/child-motor1.png)

![child-motor2](./src/child-motor2.png)

![child-motor3](./src/child-motor3.png)

![child-motor4](./src/child-motor4.png)

![child-FysioDx](./src/child-FysioDx.png)


- Taalontwikkeling (normen)
    + 1.0yo: eerste words
    + 1.5yo: 5--10 words
    + 2.0yo: 2 woordziunnen; 50% verstaanbaar
    + 3.0yo: 3 woordzinnen; 75% verstaanbaar
    + 4.0yo: meer woordzinnen, 100% verstaanbaar
        * **taalbegrip (sensor)** loopt een stap voor op **taalproductie (motor)**
         
- Sociaal-emotionele ontwikkeling
    + 0--1yo: (veilige) hechting
    + 1--3yo: eigen ik (ich, es, ueber ich)
    + 3--5yo: omgaan leeftijdsgenoten
    + 5--12yo: emotionele ontwikkeling (incl. schaamte)
    + 12--16yo: eigen identiteit

- Ontwikkelingsachterstand door DNA-mutatie
    + Dysmorfe kenmerken
        * epicanthus plooi
        * polydactyli
        * afwijkende oorschep
        * "tele..." (ogen verder uit elkaar)

- **Take-home msg**
    + de aanleg vh zenuwstelsel begint na de bevruchting en eindigt bij het einde vd puberteit
    + versch. processen vinden achtereenvolgens plaats: vorming neurale buis, celproliferatie, celmigratie, celdifferentiatie, synapsvorming, myelinisatie en geprogrammeerde celdood (apoptose?)
    + de coordinatie van deze processen ligt besloten in het DNA
    + toxische stoffen, cong. infecties en fouten in het DNA kunnen deze processen op negatieve wijze beïnvloeden
    + onderzoek van huid, haren en nagels kan belangrijke informatie opleveren over de aanleg vd hersenen ( => ectoderm/mesoderm/endoderm--related)
- **Take-home msg 2: motore mijlpalen/milestones bij kinderen per leeftijd/age**
    + rollen: 5--7mn
    + tijgeren: 6--8mn
    + kruipen: 8--9mn
    + zitten: 9mn
    + staan: 10mn
    + lopen: 12--18mn
    + rennen: 18--24mn
    + springen: 2--3y
    + hinkelen: 3--4y

### HC Motorische ontwikkeling (2020-02-18 0830) ###

- ontw. lopen: avg \~=13mn (range: 12--18mn)
	+ bij ontw. lopen: kinderen houden hun armen in typische houding (_high guard position_) om hun evenwicht te behouden tijdens het leren lopen.
- leren staan ^= 'spierkracht'
- leren lopen ^= 'evenwicht'
- `CAVE/REDFLAG`: niet kunnen lopen >2yo

- partc bij (jonge) kinderen is lastig:
	+ e.g.: @15mn => partc is bijv.: als het kind nog niet kan lopen => kan niet 'mee' met leeftijdsgenootjes die wél willen/kunnen lopen op de kinderopvang

- **buikligging**
	+ post-partum behouden kinderen nog een 'fysiologische flexiecontractuur'; ze hebben ofc. langdurig in utero gezeten
	+ dit normaliseert gedurende de eerste periode (zie ook: .pdf pp. 14--28 ; het nummer rechtsonderin op de plaatjes is de age in mn)
	+ => ze krijgen geleidelijk steeds meer extensie-capaciteit

![locomotion-pathways](./src/locomotion-pathway.png)

![locomotion-pathways](./src/locomotion-pathway.PNG)

ad. onderste 3 plaatjes zijn pathologisch
ad. => 'bridging': komt voor bij alle kinderen met achondroplasia

- "montey reflex" = Moreau reactie = ledematen spreiden
	+ wordt in het zkh meteen post-partum geetest om evt. Pl. brachialis laesie uit te sluiten (veelvoorkomend letsel tijdens de geboorte)
	+ onderscheiden neuro-letsel (Moreau reactie gestoord) vs. ClaviculaFx (Moreau reactie intact), ook veelvoorkomend letsel

- **motometrie**: meting op activiteitenniveau

- Voorbeelden van motorische tests:

|                     child motor tests                      |              |
|------------------------------------------------------------|--------------|
| GM (general movements)                                     | preterm--3mn |
| AIMS (Alberta Infant Motor Scale)                          | 0--18mn      |
| BSID-III (Bayley Scales of Infant and Toddler Development) | 0--42mn      |
| MABC-2 (Movement Assessment Battery for Children)          | 3--16yo      |

- Aandoeningsgebonden tests:
	+ GMFM
	+ MFM
	+ Vignos
	+ Brooke
	+ CMAS
	+ etc ...

- General movements ("vroeg-kinderlijke bewegingen", _NL_)
    + test domains:
        * variatie
        * complexiteit
        * vloeiendheid
        * ad. kinderen horen normaliter spontaan te bewegen als ze in rust zijn/liggen (baby's) => voorspellend voor toekomstige functie


_ _ _ _

casus 1

- Dx: ischaemic cva R hemisphere 12y ago
- Hx: atrium-septum defect waarvoor orale antistolling
- Tx:
    + Pharmx:
        * acenocoumarol v.v.
        * simvastatine 1dd20mg
- Hulpvraag:
    + ik wil langer achter elkaar kunnen lopen met minder aandacht bij het lopen
    + ik zou zonder stok en aangepast schoeisel willen lopen
- **Welke aanvullende vragen stel je om haar hulpvraag te kunnen beantwoorden?** 
    + hoe ver kan ze nu lopen
    + is er nu aanleiding waarom ze deze vraag nu heeft? (iets voorgevallen?)

- Mob:
    + stok nodig omdat ik makkelijk balans verlies
    + struikelt, kan zich onv opvangen
    + enkel zwikt snel
    + aangepaste schoenen helpen gedeeltelijk
        * tenen klauwen + pijn op bovenzijde tenen van dit schoisel
    + loopafstand max 1km
        * continu aandacht erbij houden om voet goed te plaatsen (ter preventie van vallen)
    + afgelopen jaar 2x gevallen:
        * 1x bij afstappen fiets
        * 1x bij lopen in park
            - was aan het praten en lette niet goed op. struikelde door een boomstronk
- **mogelijke oorzakelijke factoren voor loopproblemen?**
- **Welke diagnostische opties ga je nu inzetten?**
    + LO (aandachtspunten)
        * voet / gangbeeld
        * Fn-test
        * Sensibiliteit vd voet
        * spierkracht:
            - voetheffers (m.n. ook mediaal/lateraal)
            - kuit


- LO uitslag:
    + .....

- gangbeeld
    + hoofdprobleem:
        * vermidnerde klaring in zwaaifase; zowel knie- als enkelniveau
            - kuitspierspasticiteit
                * overmatige kuitspieractiviteit in zwaaifasse en licht verminderde kuitspierlengte spelen ene rol
            - kuitspiercontract
            - klauwtenen (deels structureel, deels door spasticiteit)
    * continu klauwtenen (=> pijn + extra eeltvorming)
        * verminderde kniefl in zwaaifase bij verminderde afzet
        * ten tweede sprake van milde knie-instab met hyperext 


- verminderde klaring 


- Tx options
    + vaardigheid/activiteits-training
    + pro/ortheseologie
    + neuromodulatie
    + functionele chirurgie
    + x functietraining
    + x compensatietraining...
    + `.... overnemen uit HC`

#### Casus 2 ####

- M, 50yo
- Dx: FSHD, diagnose gesteld at 30yo
- Hulpvraag:
    + minder hinder ervaren van vermoeidheid
    + langer kunnen lopen


- Ax belangrijk
    + verdelen van inspanning is lastig
    + geen sport meer momenteel
    + niet goed voeten optillen
    + regelmatig struikelen, maar 2x gevallen
    + max 45min lopen
    + extra last sinds een dag lang lopen op oneffen ondergrond in Stockholm

- LO
    - balans: sterk verstoorde balans (BBS 31/56), veel uitvalspassen nodig voor balansbehoud
    - observatie: bij langer staan treed ook regelmatig overmatige inversie op in de enkel. Dit provoceert dan de pijn t.h.v. de melleolus lat.
    - laterale voetlanding bilat, te diepe knifl tijdens loading, te weinig kie-ext in stand
- .....

- **Probleemanalyse**
    + vermoeidheid
        * passend bij FSHD
        * moeite met verdeling over de dag
        * gestopt met sporten
        * lopen kost veel aandacht
    + pijnklachten enkel rechts:
        * overbelasting bij dysbalans voetheffers en M. tib. post.
        * balansproblemen met noodzaak voor inzetten enkel-strategien
    + vermind. loopafstand + verhooogde aandachtsbelasting bij lopen:
        * minder voetheffing spierzwakte
        * verminderde afzet kracht, waardoor meer energieverbruik
        * balansproblemen

Tx belang:

- zie HC....

_ _ _ _

- stel passend behandelplan op voor casus 2

- onderbouw dit met:
    + richtlijnen
    + reviews
    + RCTs
- noem references!
- indien orthese: wat moet de orhtese doen
- in de WG bespreken we jullie plannen


### Casus 2 

~~Vrouw~~ Man, 50 jaar

- Diagnose: FSHD, diagnose gesteld op 30 jarige leeftijd

- Hulpvragen:
    + Ik wil minder hinder ervaren van mijn vermoeidheid.
    + Ik wil langer achter elkaar kunnen lopen met minder pijn.

Welke aanvullende vragen stel je om zijn hulpvraag te kunnen
beantwoorden?

### Casus 2 anamnese

> Vermoeidheid:  
> Ik heb met name last van vermoeidheid na inspanning. Het **verdelen** van inspanning over de dag vind ik lastig. Ik ben altijd sportief geweest, vooral explosieve sporten zoals basketbal of squash. Behalve wielrennen met een groep heb ik nooit echt aan duur sporten gedaan. Ik ben gestopt met wielrennen, omdat het me te veel energie en kracht kostte, het plezier ging er vanaf. Ik doe **geen sport meer op dit moment**. Wel wandelen met de hond, 2x/dag, 20-30 min. 

> Mobiliteit:  
> Ik kan maximaal 45 minuten achter elkaar lopen. Ik moet heel goed opletten dat ik mijn voeten goed **op til**. Met regelmaat **struikel** ik, gelukkig ben ik pas enkele keren gevallen. Als het druk is om mij heen, ik mijn looptempo aan moet passen, of als het terrein oneffen is, kost het lopen mij veel meer moeite. De pijnklachten rond mijn rechter enkel zijn het meest belemmerend voor hoe lang ik kan lopen. De pijnklachten zijn ontstaan na een **hele middag lopen** op glad en onregelmatig terrein (kinderkopjes in Stockholm). Eigenlijk is het nooit meer echt weggegaan.

Welke diagnostische opties ga je nu inzetten?

### Casus 2 lichamelijk onderzoek

> Spierkracht (in MRC):  
> Atrofie van de bovenbenen, met milde zwakte van de iliopsoas, en hamstrings, heupab- en adductoren (5-/5), quadriceps 4/5, links iets zwakker dan rechts. knie-extensie li 3-/5, re 4/5. Voetheffers bdz 1/5 (voornamelijk teenextensie). Voetstrekkers re 4/5 en li 2/5. M. peronei re 2/5 en li 0/5, tibialis posterior re 4/5, li 2/5 MRC.

> Gewrichtsmobiliteit:  
> Geen beperkingen/verkortingen

> Sensibiliteit:  
> Geen sensibiliteitsstoornissen aan de benen en armen

> Tonus:  
> Normotoon

> Pijn:  
> Locatie pijnklachten laterale zijde enkel net onder malleolus.
> Aanwezig bij belasten (niet in rust). Geen duidelijke tekenen van ontsteking, soms lichte zwelling.

> Functioneel:  

> Balans:  

> Sterke verstoorde balans (BBS 31/56), waardoor veel uitvalspassen
nodig zijn voor het behoud van zijn balans.

> Observatie:  
> Bij langer staan treed er ook regelmatig overmatige inversie op in de
enkel. Dit provoceert dan de pijn t.h.v. de laterale malleolus.

> Gangobservatie:  
> Beiderzijds laterale voetlanding, te diepe knieflexie tijdens loading,
veel te weinig knie-extensie in de standfase, afwezige hielheffing en
plantairflexie enkel bij verlengde bi-pedale fase, veel te weinig
voetheffing in de gehele zwaaifase, compensatoire hanantred, te
weinig knie-extensie in de late zwaaifase.

### Casus 2 Probleemanalyse

- Vermoeidheid:
    + Passend bij FSHD
    + Moeite met verdeling over de dag
    + Gestopt met sporten
    + Lopen wat veel aandacht kost

- Pijnklachten enkel rechts:
    + overbelasting bij dysbalans voetheffers en tibialis posterior
    + balansproblemen met noodzaak voor inzetten enkel-strategiën

- Verminderde loopafstand een verhoogde aandachtsbelasting bij lopen:
    + Verminderde voetheffing op basis van spierzwakte
    + Verminderde afzet kracht waardoor verhoogd energieverbruik
    + Balansproblemen

### Casus 2 Therapeutische opties

- Functietraining
- **Vaardigheids- en activiteitstraining**
- Compensatietraining
- **Pro- en ortheseologie**
- Neuromodulatie
- Functionele chirurgie
- Medicatie
- **Hulpmiddelen / aanpassingen**
- Gedragsadvies / leefregels
- Cognitieve strategietraining
- **Gedragstherapie**
- Acceptance and commitment therapy (ACS)


Aan de slag!
Stel een passend behandelplan op
Casus 1: Dianne Baltussen, Naomi van der Sluijs, Mel Visscher, Elze Braam, Susanne Bosch,
Annepien de Vries, Silke de Vreede, Jeroen de Bruin, Sanne Belder, Fleur Kuijper

Casus 2: Quentin Marsman, Anouk Peters, Linde Steinbusch, Giacomo Mazzoleni, Marie Klok
Job Scheurink, Johan van Breukelen, Karlijn Mesterom, Eva Smedema, Maaike van Campen
Onderbouw dit met:
Richtlijnen
Reviews
RCT’s
Vergeet de referenties niet te noemen!

Indien je kiest voor een orthese: wat moet deze orthese doen?
In de werkgroep bespreken we jullie plannen

#### links ####

[fysiotherapie evenwichtsproblemen](https://www.fysiodouma.nl/wiki/evenwichtsproblemen/)

[NVN - Nederlandse Vereniging voor Neurologie :: Beroerte](https://www.neurologie.nl/publiek/patientenvoorlichting/beroerte)

_ _ _ _

### WG Degeneratie

#### Casus 1

- (1) Wat zie je op deze scan?
    + normaal beeld
- (2) Welke diagnose zou je stellen op dit moment?
    + CVA -- A. cerebri media
    + uitvalsverschijnselen post-CVA
- (3) Behandelplan?
    + thrombolyse => thrombolytica acuut
    + thrombo/angioplastiek
    + `CAVE` ECG afnemen, Afib in/uitsluiten
    + farmacologisch vervolg
- (4) Predictoren voor ADL herstel
    + [X] FMA-arm index
        * vinger ext, pols ext, schouder abd => zeer bepalend
    + [X] initiele ADL is 
    + [X] rompbalans
    + incontinentie
    + [X] leeftijd => positief, pt. is nog jong (45yo)
    + eerder CVA
    + comorbiditeit
    + [X] overmoedigheid mbt capaciteit in lopen
    + [X] vermoeidheid, die hem soms plotseling overvalt
- (5) revalidatiedoelen
    + functietraining (pas 3d post-CVA)
    + mobiliteitsbehoud (/verbetering)
    + vermoeidheidslast verminderen (trainen, fysio, ergo)
    + logopedie: diagnostisch in kaart brengen situatie + beoordelen slikfunctie
    + langetermijn: psycho-educatie + psychotherapie
- (6) revalidatiekans
    + grote kans op thuis wonen na revalidatie
    + aangepaste woning is (mogelijk) nodig
    + thuiszorg
    + mantelzorg (ook afh. van partner)
    + patientfactoren (overmoedig, (plotseling) vermoeid)
    + mate van herstel

ad. 70% vd CVA-pt krijgen uiteindelijk 70% herstel vd functie
ad. neglect komt m.n. voor bij schade aan de r hemisfeer
ad. ergotherapeuten geven specialized neglect-training 

_ _ _ _

### Neuromodulatie ###

#### Casus 2 ####

- (1) wat zie je op de scan?
    + atrofie cerebrum / vergrootte ventrikels
    + `CAVE` peri-ventriculaire wittestof afw. zijn indicatief voor degeneratief proces in cerebro
- (2) diagnose?
    + CVA
- (3) behandelplan?
    + `CAVE` thrombolyse is uitgesloten indien het tijdstip vh CVA onbekend is.
    + thrombolyse heeft obv. ook een afkap-waarde


- (1) prognostische factoren
    + [X] geen rompbalans
    + [X] urine-inco
    + [X] geen handfunctie 3d post-CVA => verwachting op volledig functieverlies vd arm.
    + [X] leeftijd
    + [X] dementie
    + [X] comorbiditeit (ontwikkelend delier)
    + [X] slaapt veel, moet veel geholpen worden
- (2) ...
    + moeilijke communicatie
    + cerebrumatrofie?
    + sens. verlies minimaal vanaf knie naar
    + permanent motor loss
    + dysarthria (aphasia)
    + vallen
- (3) ...
    + delier
        * luxatiemomenten:
            - huidig CVA
            - bestaande atrofie
            - blaas overvulling
            - pneumonie
            - pijnlijke rechterbeen
            - mogelijke Fx rechterbeen?
    + decubitus
    + recidief infarct
    + spieratrofie (+ evt. resulterend functieverlies)
    + `CAVE` verslikpneumonie (resp. insuff. / O2-suppletie )
    + `CAVE` pijnlijk bij bewegen been R; mogelijk andere oorzaak?
    + ( MMSE is onuitvoerbaar )
    + 4 A's test for delirium screening
        * alert
        * orientatie plaats persoon tijd
        * concentratie
        * wisselend beloop
- (4) verklaring kracht linker lichaamshelft?
    + CVA-schade bilateraal
    + verdrukking door ruimte-innnemend proces door CVA
    + pre-existent bij deze pt
    + fysieke inactiviteit
- (5) sensibiliteit in benen?
    + slechte loopprognose
        * verminderd sens benen bilateraal
        * vibratiezin afwezig vanaf knie
    + polyneuropathie (sokvormige afwijking + afw. vibratiezin zijn veelvoorkomende tekenen bij polyneuropathie)
- (6) revalidatiedoelen?
    + delier behandelen acuut
        * prikkelarme omgeving, familie/naasten goed instrueren hierbij
        * PharmTx: haldol, antipsychotics
    + spraakfunctie verbeteren
    + inco behandelen
    + psycho-educatie voor partner
    + cerebrumatrofie in kaart brengen?
    + **ANSWER** maximaal functioneren vd linkerarm/hand vanuit rolstoel => ergo
    + **ANSWER** logopedist tbv compensatie voor aphasia en slikfunctie
- (7) leervermogen van pt?
    + minimaal (M. Alzheimer + CVA + cerebrumatrofie + complicaties post-CVA + delier)
    + **ANSWER** "foutloos leren" => in kleine stapjes aanbieden van begeleiding/hulp
- (8) verschil in manier van trainen/begeleiden tussen deze twee casus
    + Jan zal meer herstel vertonen
    + Jan zal instructies/training beter begrijpen => beter en consistent uitvoeren
    + Jan kan eerder starten met training
    + Jan kan ook hogere einddoelen stellen/behalen
    + Jan kan gemakkelijker individueel
    + Hans is cognitief aangedaan
    + Hans is ouder (80yo)
- (9) weer thuis post-revalidatie??
    + kans is minimaal
    + hoogstwaarschijnlijk (bijna) volledig ADL-afhankelijk
    + echtgenoot immobiel => beperkte mantelzorg
    + dementie
    + waarschijnlijk rolstoelafhankelijkheid

- - - -

### HC mitochondriele aandoeningen ###

#### mito syndromes ####

- MELAS (m.m3242A>G)
    + mito encephalomyopathy with lact acid acidosis and stroke-like epi's
- MIDD (m.3243A>G)
    + maternally inherited DM and deafness
- MERRF (m.MT-TK gene ; provides instructions for making tRNA molecules)
    + myoclonus epilapsy with ragged-red fibres
- S. Leigh (nuclear DNA)
    + ≥75 genes identified
    + hele typerende MRI-afw. in de basale kernen
    + multi-orgaanlijden

[^1]: flex/ext = dorsoflexie/plantairflexie
[^2]: var/valg = varus/valgus
[^3]: var(us) = "bone segment _distal to the joint_ is angled **medially**" ; valg(us) = "bone segment _distal to the joint_ is angled **laterally**"
[^4]: inversie/eversie = adductie+supinatie/abductie+pronatie
[^6]: na **aanleg/migratie** van het zenuwstelsel -> **growth/development** of: synapses, differentiation of cells (astrocytes, glial cells, oligo-dendrocytes, neurons)
[^7]: lopen ontwikkeld zich afhankelijk van ethno-geografie. Rond de evenaar is de avg. loopleeftijd ~7mn.
[^BPN]: [BPN -- UtD](https://www-uptodate-com.ru.idm.oclc.org/contents/brachial-plexus-syndromes?search=brachial%20plexus%20neuropathy&source=search_result&selectedTitle=1~150&usage_type=default&display_rank=1)

_ _ _ _


**VTA** = ventral tegmental area (_en_) = Area tegmentalis ventralis (_nl_)

`CAVE:` de VTA is qua _locatie_ AND _functie_ (AND fysiologie) nauw betrokken/gelijkend aan de **substantia nigra** ; beiden zorgen voor initiatie

- Ventral tegmental area (VTA) projections ('schakel-/regulatie-centrum voor de integratie en interactie van: geheugen-emotie-(motor)impulsactie')
    + VTA => Amygdala (verwerken van geheugen/herinneringen, beslissingen/keuzes maken, emotionele reacties (fear, anxiety, aggression)) => **lees: super primal hersenstuk voor (o.a.) fight-or-flight**
    + VTA => Cingulate gyrus (inking motivational outcomes to behavior (e.g. a certain action induced a positive emotional response, which results in learning))
    + VTA => Hippocampus (short+long-term memory, spatial memory ('geheugenfuncties verantwoordelijk voor 'sensen'/interpretatie van informatie uit je omgeving (spatial orientation)') => helpt in het vormen van geheugen voor orientatie (_cognitive map_ = cognitieve landkaart))
    + VTA => Nucleus accumbens (_NAc or NAcc_) => versch. cognitieve functies (motivatie, afkeer (aversie), beloning, pos. bekrachtiging, leren door bekrachtiging, verslaving) ;; super-dopaminerg. 
    + VTA => Olfactory bulb (ruikruik)
    + VTA => Prefrontal cortex ((uiting van) persoonlijkheid, 'beslissen' tot actie, planning of complex cognitive behaviour, moderating social behaviour and speech (aka: 'onderdrukken van ongewenst gedrag' => **lees: impulscontrole en persoonlijkheidsverandering** zoals bij fronto-temporale dementia)


- pt. A
    + centrale facialis paresis
    + latent paresis? => parese openbaart zich situationeel ( lees: uitzakken (parese) hand bij Barre test (situationeel) ) => dus parese is 'latent'

- pt. B: lacunair infarct ACM RH
    + links vermind aansturing
    + fijne motoriek verm
    + regelmatig emotioneel
    + coping gestoord
        * doel: weer lopen, psychische acceptatie

- pt. C: iCVA LH
    + voetheffersparese R (bij lopen)
    + balansproblemen
    + coordinatieproblemen R arm
    + licht dysarthria
    + cogn. ongestoord
    + premorbide depressie (post-breakup 1y ago)
    + ....

- pt. D: basal ganglia hCVA L
    + aphasia
    + hemiparalyse
    + sens stoornis
    + kortetermijn geheugen
    + verm aandacht voor R

### HC neurale adaptatie (2020-03-03)

#### Intro

- model: CVA
- problem: many theoretic approaches in stroke rehab (e.g.: Affolter, Vojta, P.N.F., Johnstone, M.R.P., Brunnstromm)
    - NDT: neuro-developmental treatment
        - speerpunt was symmetrie:
            - bewegen werd afgeraden, indien beweging niet symmetrisch kon worden uitgevoerd
            - `CAVE`: hersteelvermogen is niet altijd hoog genoeg voor herstel tot volledig symmetrisch functioneren => dan: wachten met bewegen tot volledige symmetrie is behaald (**vertraagd dus het herstel**)
    - little clinical evidence
    - little fundamental insight in functional recovery
        - **There is no evidence of a surplus value of one neurologically oriented training method over the other in stroke rehab.**

- Neural repair => restitution of function
- Neural compensation => substitution of function
    - res + sub of func => skill acquisition

- **FAC-score** (functional ambulation score):
    - scoort zelfstandig lopen
    - 5 (volledig zelfstandig lopen ==//==> 0 (niet lopen)

- _"To what extent **can** and **should** postural symmetry be promoted after stroke?"_

#### Postural instability

- in SMK onderzocht
    - voor het eerst (2004) werd het mogelijk om postural instability _voor elk been onafhankelijk_ te meten.

#### Balance study

**TEKENINGEN TOEVOEGEN???**

- statische assymetrie: staande assymetrie
- dynamische assymetrie: corrigerende bewegingen assymetrie

- Summary of balance study:
    - after supratentorial stroke:
        - clinical balance considerably improves (e.g. FAC, BBS)
        - frontal plane standing balance most strongly affected
        - frontal > saggital plane standing balance improves in time
        - **but... static and dynamic asymmetry persist to a substantial degree**
- Conclusion balance study:
    - **An important part of standing balance recovery from stroke takes place _independent_ of:**
        - improved static weight bearing on the paretic leg
        - improved dynamic contribution by the paretic leg
        - _Especially true in pts. with severe leg paresis_
- **Even in well-recovered communicty-dwelling chronic stroke pts, large differences are found between the dynamic contribution of each leg to balance control)**

- Balance study was redone, results:
    - static assymm. 60% / 40%
    - dynam assymm.  90% / 10%
        - (perc are noted as: correction by non-paretic leg / correction by paretic leg)

- Re-study: EMG-activity vs. functional recovery
- Results:
    - EMG-patterns remain unchanged, even if functionality increases
    - Even if one-legged function remains unchanged; two-legged function increaes and total/eventual balance outcome is better.

- **Men denkt dat de resultaten niet-afwijkend zijn, omdat de rompsieren zowel cerebraal als spinaal bilateraal geinnerveerd zijn.**


#### Bottom line:

- **compensatory mechanisms play a major role** in the recovery of balance and gait after stroke => this notino questions the prolonged emphasis of some approaches on **restitution of function** (BobachTx = NDT)

#### Post-stroke balance and gait framework

![post-stroke balance and gait framework](./src/post-stroke-bal-gait-framework.png)

ad. restoration of axial muscle functiopnality (probably) relies/is facilitated by bilateral innervation of the axial muscles (trunk muscles)

### WG Neurale adaptatie 2

- ARAT-score: vaardigheidsscore
- FM-arms/hands: selectiviteit vd bewegingen
    - => Kwakkel e.a., Restor Neurol Neurosci. 2007
        - FM-hand score is predictief voor vaardigheidsherstel (functietest voorspelt vaardigheid)
- Wrist ext + Finger ext zijn de beste tests voor het bepalen **of de Tr. Corticospinalis** intact is
    - deze klinische test is gelijkwaardig aan tests m.b.v. TMS
    - (e.g. wrist flex is also innervated by Tr. Reticulospinalis)

![spont-neur-recov](./src/spont-neur-recov.png)

### Penumbra vs. diaschisis

![penumbra-diaschisis](./src/penumbra-diaschisis.png)

> **Diaschisis**: "Diaschisis (from Greek διάσχισις meaning "shocked throughout"[1]) is a sudden change of function in a portion of the brain connected to a distant, but damaged, brain area.[2] The site of the originally damaged area and of the diaschisis are connected to each other by neurons.[3]" (wiki)

### Proportional recovery rule UE

- majority of pts (n=146;~70%) show a fixsed proportional upper extremity motor recovery of about 78%
- 65 pts (non-fitters) show substantially less improvement than predicted
- Non-fitters have more severe neurological impairments within 72 hours post-stroke.
- Logistic regression analysis reveal sthat absence of **finger ext, presence of facial palsy, more severe lower extremity paresis and more sever type of stroke** are significant predictors of not fitting the proportional recovery model.
    - Prop. recov. rule extended:
        - lower extremity (no non-fitters)
        - aphasia
        - visuospatial neglect

- mCIMT: (modified) Constraint-induced movement therapy
    - Gemodificeerde CIMT is minder intensief
    - De originele CIMT die is ontwikkeld (tijdens onderzoek), bleek in de praktijk een te intensief protocol en niet haalbaar, daarom is de CIMT later gemodificeerd.
    - voor spina bifida is het handigste om kinderneurologie.eu te gebruiken en vooral te vergelijken met dwarslaesie..

### Recovery of function

![stroke-recov](./src/stroke-recov.png)

- extent of true recovery is limiited by the intrinsic and definitive damage to the Tr. Corticospinal ('restitution of function') => i.e. normal motor control
- Task-related activity gradually shifts from primary to secondary neural networks, but secondary networks often provide less selective and less efficient motor control (e.g. Tr. Reticulospinalis or Tr. Rubrospinalis) ('substitution of function') => e.g. a slower and more grossly controlled upper extremity
- Adaptive control strategies allow for alternative ways of controlling the same movement ('substitution of function') => e.g. assymetric balance control
- Alternative mvoement strategies allow for other ways to reach the same goal ('compensation') => e.g. using a different extremity or adaptive tool.

### Logopedie

#### Dysphagia

![ICF-dysphagia](https://www.dysfagie.info/images/ICF_model_EN(1).jpg)

### HC FysioTx (post-CVA)

#### Diagnostiek

- vaststellen en monitoren niveau van functioneren
- risico-assessment op het ontstaan van complicaties
    - pneumothorax
    - decubitus
    - contracturen
    - schouderpijn
    - delirium

#### Behandelprincipes

- intensiteit vd oefenTx
- taakspecifiek
- contextspecifiek
- volgens de principes van motorisch leren

#### Diagnostiek: vaststellen niveau van functioneren

- anamnese
- LO functiestoornissen
- klinimetrie (o.a. motricity index, trunk control test, Berg balance scale, mini best test)
- maar ook activiteiten in het dagelijkse leven observeren
    - hoe verplaatst pt. zich van A naar B?
    - hoe voert hij dagelijkse activiteiten uit (schoenen aantrekken, huishouden, etc)

#### Doelen afhankelijk van fase

- hyperacute fase: 0--24h
- vroege revalidatiefase: 24--72h
    - aanleren adaptatiestrategieen
- late revalidatiefase 3--6mn
    - verminderen beperkingen in activiteiten en participatie
- chronische fase: >6mn
    - leren omgaan met beperking, mataschappelijk functioneren, behoud fysieke belastbaarheid

#### Tx: acute fase

- vroegtijdige mobilisatie <24h
    - verlaagd risico op extracerebrale complicatie
    - de kans op overlijden verminderd
    - het functionele herstel en ADL-zelfstandigheid versnelt en verbeterd
    - pt gaan sneller met ontslag
- `CAVE:` er is indirect bewijs dat vroegtijdig mobiliseren binnen 24h post-stroke nadelig kan zijn voor pts. met ernstige neurologische uitval (NIHSS>16) bij opname op een stroke-unit

#### Intensief oefenen

- intensief oefenen leidt tot sneller herstel van:
    - selectiviteit van bewegen
    - comfortabele en maximale loopsnelheid
    - zit en stabalans
    - uitvoer van basale activiteiten van dagelijks leven
    - kwaliteit van leven
- **er lijkt geen duidelijk plafondeffect**
- De richtlijn CVA adviseert een minimum van oefenen: 45m/d, 7d/w

### HC FysioTx pt.2 ###

- bedoeling vh HC is vragen stellen die wij willen weten van Maaike/CVA-pt als toekomstig arts.

### CVA Revalidatie FysioTx ###

#### Ervaringen van een patiënt ####

- CVA-pt. 
- ...
- op de eerste hulp 
- ze zeiden eerst: conversie => na de scan bleek het een hCVA.
    + "voelde je eerst niet 'niet serieus genomen'" ; of was daar wellicht geen aandacht/tijd voor op dat moment?
- graag meer vertellen over 'de operatie' waar je voor in aanmerking komt
- bleek uiteindelijk een AVM te zijn, waarschijnlijk niet-aangeboren
    + zat wel op een locatie die operabel was (operatie was 5mn post-CVA) => AVM verwijdert.
- post-CVA is Maaike motoor nog niet geheel hersteld ; ze deed eerst korfbal, maar dat kan niet meer. Hardlopen gaat bijna niet meer => nu doet ze rolstoelbasketball

- individueel
    + fysioTx
    + ergoTx
    + verpleegkundigen
    + PMT (psychomotorische Tx, sportTx)
    + muziekTx
    + _zelf oefenen - oefengids_

- groepsverband
    + ochtendTx 
        * al lopend naar ochtendTx ; zelf je bord pakken uit een kastje, zelf je bestek, zelf je brood smeren, eten (hele functionele training)
    + loopprognosep
    + handengroep
    + tafeltje-dekje
    + fitstroke
    + fitness
    + hydroTx
    + _oefenen met maatje_

- "Hoe dacht je over je herstel in het begin vs. je herstel nu?"
    + Ze weet nog dat ze in het ziekenhuis lag en zag een meisje dat prima revalideerde => dit bleek snel niet te kloppen
    + ze is toen zelf vnl. 'week voor week' gaan leven, ook als een soort copingsstrategie
    + 4mn post-CVA was het motoor/sensorisch redelijk goed hersteld => pas 0.75--1 jaar daarna is er veel herstel in belastbaarheid gekomen en dat was de stimulerende factor voor haar.
    + "voor de functionaliteit had ze niet getekend, maar als het gaat om de belastbaarheid zoals ze die nu heeft, dan zeker wel"
- "Hoe is het om zo ineens al je privacy te verliezen en hoe ga je er dan mee om?"
- "Restproblematiek?"


- Thuisarts.nl (NHG) - Keuzehulp => filmpje voor armtraining door Maaike?
- betrokken bij pt-vereniging + wetenschappelijk onderzoek + schrijft nu artikel

_ _ _ _

### HC Probleemanalyse en plan/beleid

#### Diagnostische opties

- Motorische diagnostiek (klinisch)
    + Orthopaedisch onderzoek (e.g. Cyriax methode)
        * oorzaak pijn
        * oorzaak dysfunctie (focus = pees, gewricht, bot)
    + Onderscheid:
        * passieve tests => gewricht / bot (pROM)
        * weerstandstesten => pees (een enkele keer aROM)

- Orthopaedisch onderzoek (Cyriax)
    + **Capsulair patroon: wijzend op 'inflammatie' gewricht**
    + oorzaken: septisch, reumatisch, degeneeratief, trauma, immobilisatie
    + Patronen van bewegingsbeperking:
        * shld: exo > abd > endo
        * elbw: flx > ext
        * wrst: flx = ext
        *  hip: endo > flx = abd > ext
        * knee: flx >> ext > rot
        * ankl: pfl > dfl
        * subtal: var > val

- Motorische diagnostiek (klinisch)
    + Neurologisch onderzoek
        * oorzaak dysfunctie (focus = zenuw, spier)
    + Onderscheid:
        * sensibiliteit
        * reflexen en tonus
        * spierkracht en selectiviteit
        * spiercoordinatie en spiersnelheid
        * functionele tests (balans, lopen, wijzen)
        * **ook pROM kennen!**

- Klinisch-neurofysiologisch onderzoek
    + aanvullend onderozek van zenuw en spier:
        * EMG (electromyografie)
        * TMS -> MEP (motor evoked potential)
        * SSEP (somatosensory evoked potential)
        * spierechografie

- Beeldvorming
    + Aanvullend onderzoek van pees, gewricht, bot, zenuw, spier:
        * x-ray / rontgen (skelet)
        * CT (skelet, spier)
        * MRI (zenuw, spier, pees)
        * echografie (pees, spier, zenuw)
        * botscan (skelet)

- Instrumentele motorische diagnostiek (bewegingslab)
    + aanvullend bewegingsonderzoek:
        * gangbeeldanalyse (3-D, kinetica, sEMG)
        * slikvideo (videofluoroscopie)
        * echo en sEMG van slikspieren

- Instrumentele autonome diagnostiek
    + aanvullend autonoom functieonderzoek:
        * blaasecho
        * UDO (urodynamisch onderzoek)
        * dynamisch bloeddrukonderzoek (orthostase)

- spraak-taaldiagnostiek (logopedist)
- cognitieve diagnostiek (neuropsycholoog)
- gedragsobservatie (ADL) (verpleging)

#### Therapeutische opties

- Functietraining
    + krachttraining
    + training motorische selectiviteit

- Vaardigheids- en activiteitstraining
    + looptraining
    + fitness training

- Gait adaptability training
    + GRAIL
    + loopband met obstakels (ForceLink C-Mill)

- Compensatietraining
    + eenhandigheidstraining
    + rolstoeltraining

- Pro- en orhteseologie
    + prothese (prosthesis, _EN_) / osseo-integratie
    + enkel-voetorthese (ankle-foot prosthesis (AFO), _EN_)
    + exoskelet

- Neuromodulatie
    + blaasstimulatie
    + ruggenmergstimulatie
    + functionele electrostimulatie 
    + externe peroneusstimulatie (L300)

- Functionele chirurgie
    + enkel-voet chirurgie
    + multilevel chirurgie
    + scoliosechirurgie
    + chirurgische varuscorrectie

- PharmacoTx
    + spasmolytica, pijnstillers per ons
    + baclofen intrathecaal (intrathecal baclofen (ITB), _EN_)
    + BTX-A I.M.
    + corticosteroids intra-articular

- Hulpmiddelen / aanpassingen
    + handstok / rollator
    + rolstoel
    + 3-wiel E-bike

- Gedragsadvies / leefregels
    + energiemanagement
    + slaaphygiene
    + lifestyle verandering

- Cognitieve strategietraining
    + geheugenstrategieen
    + visuele orientatiestrategieen
    + telegramspraak

- CBT (Cognitive Behavioural Therapy)
    + gedachten en gedrag rond chronische pijn of moeheid veranderen
    + impulsregulatie

- Acceptance and commitment therapy (ACS)
    + accepteren van nieuwe situatie en mogelijkheden




[Oral muscles deteriorate in DMD article](https://www.ncbi.nlm.nih.gov/pubmed/23263593)


_ _ _ _

## TENTAMEN


- [LINK NAAR TENTAMEN](https://brightspace.ru.nl/d2l/le/content/115281/Home)


## ARTIKEL

- [Huijben J Child Neurol 2015](https://doi.org/10.1177/0883073814534316)

- pt.: 9yo girl, ambulatory
- Dx: dystrophinopathy due to mosaic translocation mutation
- article goal: No curative treatment for DMD, bu timproved health care has increased life expectancy in the Western world. Muscle training regimens associated with **high levels of mechanical stress can be harmful** in the setting of dystrophin deficiency, whereas **low-stress exercise can produce beneficial effects** on myofiber contractility and energetic deficiency.
- Tx/Intervention: "... received assisted bicycle training for her legs and arms. \[...] We assessed **whether this training was also safe and beneficial in this girl with dystrophinopathy**"
- outcome: "the results of this case report provide some insight into the influence of a certain level of dystrophin on the effects of exercise"
- H0: "nonstrenuous exercise can potentially be used to avoid disuse atrophy and other secondary complications of physical inactivity"

- Outcome parameters
    + primary:
        * Motor Function Measure (MFM)
        * Assisted Six-Minute Cycling Test (A6MCT)
            - trained at home, 15min with legs followed by her arms; 5x/w at assistance level passive mode 1 (i.e., no-load, speed 7 revolutions/min). Training intensity was low-to-moderate at established with the **OMNI scale of perceived exertion** (max OMNI score = 6)
    + secondary
        * Vignos + Brook scale (for LE and UE functioning)
        * Timed tests
            - time to rise from a floor
            - .. to rise from a chair
            - .. to climb 3 stairs
            - .. to walk 10m
        * MRC for muscle strength
        * quantitative muscle US to determinen echo intensity of:
            - M. biceps brachii
            - forearm flexors
            - M. quadriceps, rectus femoris
            - M. tibialis ant
            - **echo intensities are presented as z scores, that is, the number of standard deviations from the mean of the reference group**
                + (in boys with DMD, echo intensities increase with age and as the disease severity worsens because of muscle fibrosis)

- 
